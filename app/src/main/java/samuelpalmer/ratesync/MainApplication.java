package samuelpalmer.ratesync;

import android.app.Application;
import android.content.Context;
import android.os.Process;
import android.os.StrictMode;
import android.util.Log;

import samuelpalmer.ratesync.processes.service.Notifications;
import samuelpalmer.ratesync.processes.ui.ServiceStateSynchroniser;
import samuelpalmer.ratesync.processes.ui.SettingsSynchroniser;
import samuelpalmer.ratesync.settings.Upgrader;

@SuppressWarnings("FieldCanBeLocal") // Storing objects in fields to ensure they aren't garbage-collected, which can break some subscriptions
public class MainApplication extends Application {

    private static final String TAG = MainApplication.class.getSimpleName();

    private ServiceStateSynchroniser serviceStateSynchroniser;
    private SettingsSynchroniser settingsSynchroniser;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        // Application.onCreate() is called *after* ContentProvider.onCreate().
        // However, attachBaseContext() is called before it, which is why this method is used here.
        Log.i(TAG, "Process created: " + CurrentProcess.getCurrentProcessName());

        /*
         * Sometimes Android makes two instances of Application and its components within the one process.
         * When this happens, the whole process is effectively corrupt and unusable, so we'll just kill it
         * so it doesn't crash and generate an error report.
         *
         * https://issuetracker.google.com/issues/36972466#comment14
         */
        if (getResources() == null) {
            Log.w(TAG, "Process is in a bad state. Killing process.");
            Process.killProcess(Process.myPid());
        }
    }

    @Override
    public void onCreate() {
        if (BuildConfig.DEBUG)
            configureDebug();

        super.onCreate();

        // Note that crashes that occur here don't appear to prevent the rest of the application from running, at least on Android 7.1 (not sure about other OS versions)

        /*
         * Must setup notification channels before doing anything else so channels will exist when notifications are posted.
         * Doing this regardless of which process we're in since notifications can (technically) be posted from either process.
         * We cannot rely on the app settings to determine whether this is needed since the app settings could have been
         * migrated from a different device.
         */
        Notifications.createServiceChannel(this);

        // Must upgrade settings before any settings-related code runs.
        Upgrader.upgradeAppSettings(this);

        if (!CurrentProcess.isServiceProcess(this)) {
            // Our process is just starting up, so we'll ensure the service is running if needed in
            // case it was killed by a device restart, app upgrade, crash, or the user
            serviceStateSynchroniser = new ServiceStateSynchroniser(this);
            serviceStateSynchroniser.ensureStarted();

            // Listening for app settings changes here so that they will always be synchronised to
            // the service process
            settingsSynchroniser = new SettingsSynchroniser(this);
            settingsSynchroniser.ensureStarted();
        }
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        // This isn't called on real devices, so there's no need to do cleanup work here.

        Log.i(TAG, "Process terminated");
    }

    /**
     * Applies configuration to the current process to assist local development and debugging.
     * This should not be run in Production.
     */
    private static void configureDebug() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .permitDiskReads() // So we can read SharedPreferences and the current process name
                .permitDiskWrites() // So SharedPreferences can initialise itself on first-run
                .penaltyLog()
                // Not using penaltyDialog since it's too annoying
                .build()
        );

        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                // Not using penaltyDeath since it makes it look like the app is malfunctioning
                .build()
        );
    }

}
