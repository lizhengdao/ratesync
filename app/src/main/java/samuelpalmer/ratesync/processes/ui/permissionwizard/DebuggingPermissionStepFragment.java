package samuelpalmer.ratesync.processes.ui.permissionwizard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.leanback.app.GuidedStepSupportFragment;
import androidx.leanback.widget.GuidanceStylist;
import androidx.leanback.widget.GuidedAction;

import java.util.List;

import samuelpalmer.ratesync.R;
import samuelpalmer.ratesync.utilities.ActivityUtils;
import samuelpalmer.ratesync.utilities.AdbPermissionCheck;
import samuelpalmer.ratesync.utilities.DeveloperOptionsEnabledStateMonitor;
import samuelpalmer.ratesync.utilities.NetworkDebuggingEnabledStateMonitor;
import samuelpalmer.ratesync.utilities.eventhandling.EventHandler;

/**
 * Prompts the user for the permissions needed to use ADB
 */
public class DebuggingPermissionStepFragment extends GuidedStepSupportFragment {

    private static final int ACTION_ID_DEVELOPER_OPTIONS = 1;
    private static final int ACTION_ID_NETWORK_DEBUGGING = 2;
    private static final int ACTION_ID_ALLOW_DEBUGGING = 3;

    private DeveloperOptionsEnabledStateMonitor developerOptionsMonitor;
    private NetworkDebuggingEnabledStateMonitor networkDebuggingMonitor;
    private AdbPermissionCheck adbPermissionCheck;

    private boolean canUpdateFragments;
    private boolean finishedOverlyingActivities;
    private boolean movedToNextStep;
    private boolean hasAllowDebugging;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        canUpdateFragments = false;
        finishedOverlyingActivities = true;
        movedToNextStep = false;
        hasAllowDebugging = false;

        developerOptionsMonitor = new DeveloperOptionsEnabledStateMonitor(context);
        networkDebuggingMonitor = new NetworkDebuggingEnabledStateMonitor(context);
        adbPermissionCheck = new AdbPermissionCheck();

        developerOptionsMonitor.subscribe(developerOptionsChanged);
        networkDebuggingMonitor.subscribe(networkDebuggingChanged);
        adbPermissionCheck.onResult.subscribe(adbPermissionChanged);

        permissionsChanged.update();
    }

    @Override
    public void onDetach() {
        super.onDetach();

        adbPermissionCheck.ensureStopped();

        adbPermissionCheck.onResult.unsubscribe(adbPermissionChanged);
        networkDebuggingMonitor.unsubscribe(networkDebuggingChanged);
        developerOptionsMonitor.unsubscribe(developerOptionsChanged);

        networkDebuggingMonitor = null;
        developerOptionsMonitor = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        canUpdateFragments = true;
        permissionsChanged.update();
    }

    @Override
    public void onStop() {
        super.onStop();
        canUpdateFragments = false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        canUpdateFragments = false;
    }

    private final EventHandler developerOptionsChanged = new EventHandler() {
        @Override
        public void update() {
            if (developerOptionsMonitor.isEnabled())
                tryFinishOverlyingActivities();

            permissionsChanged.update();
        }
    };

    private final EventHandler networkDebuggingChanged = new EventHandler() {
        @Override
        public void update() {
            if (networkDebuggingMonitor.isEnabled())
                tryFinishOverlyingActivities();

            permissionsChanged.update();
        }
    };

    private final EventHandler adbPermissionChanged = new EventHandler() {
        @Override
        public void update() {
            hasAllowDebugging = adbPermissionCheck.isPermanentlyAuthorised();
            permissionsChanged.update();
            adbPermissionCheck.ensureStopped();
        }
    };

    private final EventHandler permissionsChanged = new EventHandler() {
        @Override
        public void update() {
            boolean hasDeveloperOptions = developerOptionsMonitor.isEnabled();
            boolean hasNetworkDebugging = networkDebuggingMonitor.isEnabled();

            // We don't know whether the actions have been added yet, so we'll check before updating their statuses
            if (!getActions().isEmpty()) {
                GuidedAction developerOptionsAction = findActionById(ACTION_ID_DEVELOPER_OPTIONS);
                GuidedAction networkDebuggingAction = findActionById(ACTION_ID_NETWORK_DEBUGGING);
                GuidedAction allowDebuggingAction = findActionById(ACTION_ID_ALLOW_DEBUGGING);

                developerOptionsAction.setEnabled(!hasDeveloperOptions);
                developerOptionsAction.setFocusable(!hasDeveloperOptions);

                networkDebuggingAction.setEnabled(hasDeveloperOptions && !hasNetworkDebugging);
                networkDebuggingAction.setFocusable(hasDeveloperOptions && !hasNetworkDebugging);

                allowDebuggingAction.setEnabled(hasNetworkDebugging && !hasAllowDebugging);
                allowDebuggingAction.setFocusable(hasNetworkDebugging && !hasAllowDebugging);

                // Now that we've made some changes, we'd better let the fragment know so it can update
                notifyActionChanged(findActionPositionById(ACTION_ID_DEVELOPER_OPTIONS));
                notifyActionChanged(findActionPositionById(ACTION_ID_NETWORK_DEBUGGING));
                notifyActionChanged(findActionPositionById(ACTION_ID_ALLOW_DEBUGGING));

                // We might have disabled the selected action, so we need to set a new selection
                GuidedAction actionToSelect;
                if (allowDebuggingAction.isEnabled())
                    actionToSelect = allowDebuggingAction;
                else if (networkDebuggingAction.isEnabled())
                    actionToSelect = networkDebuggingAction;
                else if (developerOptionsAction.isEnabled())
                    actionToSelect = developerOptionsAction;
                else {
                    // The user has just granted the last permission,
                    // so we don't want to move the selection since
                    // the dialog is about to close anyway
                    actionToSelect = null;
                }

                if (actionToSelect != null)
                    setSelectedActionPosition(findActionPositionById(actionToSelect.getId()));
            }

            if (hasDeveloperOptions && hasNetworkDebugging && hasAllowDebugging && !movedToNextStep && canUpdateFragments) {
                finishGuidedStepSupportFragments();
                movedToNextStep = true;
            }
        }
    };

    private void tryFinishOverlyingActivities() {
        FragmentActivity activity = getActivity();
        if (activity != null && !finishedOverlyingActivities) {
            ActivityUtils.finishActivitiesOnTopOf(activity);
            finishedOverlyingActivities = true;
        }
    }

    @NonNull
    @Override
    public GuidanceStylist.Guidance onCreateGuidance(Bundle savedInstanceState) {
        String title = getString(R.string.permission_debugging_title);
        String description = getString(R.string.permission_debugging_explanation, getString(R.string.app_name));
        String breadcrumb = getString(R.string.permission_title);
        return new GuidanceStylist.Guidance(title, description, breadcrumb, null);
    }

    @Override
    public void onCreateActions(@NonNull List<GuidedAction> actions, Bundle savedInstanceState) {
        super.onCreateActions(actions, savedInstanceState);

        GuidedAction developerOptionsAction = new GuidedAction.Builder(requireContext())
                .id(ACTION_ID_DEVELOPER_OPTIONS)
                .title(R.string.permission_debugging_action_developer)
                .hasNext(true)
                .build();

        GuidedAction networkDebuggingAction = new GuidedAction.Builder(requireContext())
                .id(ACTION_ID_NETWORK_DEBUGGING)
                .title(R.string.permission_debugging_action_network)
                .hasNext(true)
                .build();

        GuidedAction allowDebuggingAction = new GuidedAction.Builder(requireContext())
                .id(ACTION_ID_ALLOW_DEBUGGING)
                .title(R.string.permission_debugging_action_allow)
                .description(R.string.permission_debugging_instructions_allow)
                .hasNext(true)
                .build();

        GuidedAction cancelAction = new GuidedAction.Builder(requireContext())
                .clickAction(GuidedAction.ACTION_ID_CANCEL)
                .build();

        actions.add(developerOptionsAction);
        actions.add(networkDebuggingAction);
        actions.add(allowDebuggingAction);
        actions.add(cancelAction);
    }

    @Override
    public void onGuidedActionClicked(GuidedAction action) {
        super.onGuidedActionClicked(action);

        long id = action.getId();
        Activity activity = requireActivity();

        if (id == ACTION_ID_DEVELOPER_OPTIONS) {
            activity.startActivity(new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS));
            finishedOverlyingActivities = false;
            Toast.makeText(activity, R.string.permission_debugging_instructions_developer, Toast.LENGTH_LONG).show();
        } else if (id == ACTION_ID_NETWORK_DEBUGGING) {
            activity.startActivity(new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS));
            finishedOverlyingActivities = false;
            Toast.makeText(activity, R.string.permission_debugging_instructions_network, Toast.LENGTH_LONG).show();
        } else if (id == ACTION_ID_ALLOW_DEBUGGING) {
            adbPermissionCheck.ensureStarted();
        } else if (id == GuidedAction.ACTION_ID_CANCEL) {
            finishGuidedStepSupportFragments();
        }
    }

}
