package samuelpalmer.ratesync.processes.ui.permissionwizard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.leanback.app.GuidedStepSupportFragment;
import androidx.leanback.widget.GuidanceStylist;
import androidx.leanback.widget.GuidedAction;

import java.util.List;

import samuelpalmer.ratesync.R;
import samuelpalmer.ratesync.utilities.ActivityUtils;
import samuelpalmer.ratesync.utilities.eventhandling.EventHandler;
import samuelpalmer.ratesync.utilities.eventhandling.EventRegistrar;

/**
 * Template for a permission screen that has prompts the user to grant a single
 * permission
 */
abstract class PermissionStepFragment extends GuidedStepSupportFragment {

    private static final long ACTION_ID_ALLOW = 1;

    private boolean canUpdateFragments = false;
    private boolean finishedOverlyingActivities = false;
    private boolean movedToNextStep = false;

    @NonNull
    @Override
    public GuidanceStylist.Guidance onCreateGuidance(Bundle savedInstanceState) {
        String title = getString(getTitleResourceId());
        String description = getString(getExplanationResourceId(), getString(R.string.app_name));
        String breadcrumb = getString(R.string.permission_title);
        return new GuidanceStylist.Guidance(title, description, breadcrumb, null);
    }

    @StringRes protected abstract int getTitleResourceId();
    @StringRes protected abstract int getExplanationResourceId();
    protected abstract EventRegistrar onPermissionChanged();
    protected abstract boolean hasPermission();
    protected abstract Intent getPromptIntent();
    protected abstract CharSequence getInstructions();
    protected abstract GuidedStepSupportFragment getNextStep();

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        onPermissionChanged().subscribe(checkPermission);
        checkPermission.update();
    }

    @Override
    public void onDetach() {
        super.onDetach();

        onPermissionChanged().unsubscribe(checkPermission);
    }

    @Override
    public void onStart() {
        super.onStart();
        canUpdateFragments = true;
        checkPermission.update();
    }

     @Override
     public void onStop() {
         super.onStop();
         canUpdateFragments = false;
     }

     @Override
     public void onSaveInstanceState(Bundle outState) {
         super.onSaveInstanceState(outState);
         canUpdateFragments = false;
     }

     private final EventHandler checkPermission = () -> {
        boolean hasPermission = hasPermission();
        if (!hasPermission)
            return;

        FragmentActivity activity = getActivity();
        if (activity != null && !finishedOverlyingActivities) {
            ActivityUtils.finishActivitiesOnTopOf(activity);
            finishedOverlyingActivities = true;
        }

        if (!canUpdateFragments) {
            // We're not allowed to commit a fragment transaction at this stage, so we'll leave it for later on
            return;
        }

        if (!movedToNextStep) {
            GuidedStepSupportFragment nextStep = getNextStep();

            if (nextStep == null) {
                finishGuidedStepSupportFragments();
            } else {
                FragmentManager fragmentManager = requireFragmentManager();
                add(fragmentManager, nextStep);
            }

            movedToNextStep = true;
        }
    };

    @Override
    public void onCreateActions(@NonNull List<GuidedAction> actions, Bundle savedInstanceState) {
        super.onCreateActions(actions, savedInstanceState);

        GuidedAction allowAction = new GuidedAction.Builder(requireContext())
                .id(ACTION_ID_ALLOW)
                .title(R.string.permission_action_allow)
                .description(getInstructions())
                .hasNext(true)
                .build();

        GuidedAction cancelAction = new GuidedAction.Builder(requireContext())
                .clickAction(GuidedAction.ACTION_ID_CANCEL)
                .build();

        actions.add(allowAction);
        actions.add(cancelAction);
    }

    @Override
    public void onGuidedActionClicked(GuidedAction action) {
        super.onGuidedActionClicked(action);

        long id = action.getId();
        if (id == ACTION_ID_ALLOW) {
            Intent intent = getPromptIntent();
            Activity activity = requireActivity();
            activity.startActivity(intent);

            CharSequence instructions = getInstructions();
            Toast.makeText(activity, instructions, Toast.LENGTH_LONG).show();
        } else if (id == GuidedAction.ACTION_ID_CANCEL) {
            finishGuidedStepSupportFragments();
        } else {
            throw new RuntimeException("Unknown action clicked: " + id + " - " + action.getTitle());
        }
    }

}
