package samuelpalmer.ratesync.processes.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

/**
 * Used to switch activity away from the activity that is currently playing media. This works around
 * issues that can occur if the refresh rate is switched while the media is playing.
 *
 * Lives in the UI process to keep the service process's memory usage low.
 */
public class RefreshRateSwitcherActivity extends Activity {

    private static final String TAG = RefreshRateSwitcherActivity.class.getSimpleName();

    private boolean isCreated;

    /**
     * Sends a broadcast to any running instance of the activity requesting it to finish.
     */
    public static void requestFinish(Context context) {
        Intent intent = new Intent(makeBroadcastAction(context));
        context.sendBroadcast(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntentFilter filter = new IntentFilter();
        filter.addAction(makeBroadcastAction(this));
        registerReceiver(broadcastReceiver, filter);

        isCreated = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        isCreated = false;

        unregisterReceiver(broadcastReceiver);
    }

    private static String makeBroadcastAction(Context context) {
        return context.getPackageName() + ".action.FINISH_SWITCHER_ACTIVITY";
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isCreated) {
                Log.w(TAG, "Received broadcast when not created. Ignoring.");
                return;
            }

            if (isFinishing()) {
                Log.w(TAG, "Received request to finish activity, but is already finishing. Ignoring.");
                return;
            }

            Log.i(TAG, "Received request to finish activity. Finishing.");
            finish();

            // Not animating when we transition back into the media app so we'll make things a bit
            // faster for the user
            overridePendingTransition(0, 0);
        }
    };

}
