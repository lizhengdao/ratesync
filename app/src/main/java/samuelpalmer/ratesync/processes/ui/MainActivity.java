package samuelpalmer.ratesync.processes.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import samuelpalmer.ratesync.AllPermissionsMonitor;
import samuelpalmer.ratesync.BuildConfig;
import samuelpalmer.ratesync.R;
import samuelpalmer.ratesync.processes.ui.permissionwizard.PermissionWizardActivity;
import samuelpalmer.ratesync.utilities.AdbPermissionCheck;
import samuelpalmer.ratesync.utilities.DeviceInfo;
import samuelpalmer.ratesync.utilities.eventhandling.EventHandler;

public class MainActivity extends FragmentActivity {

    private static final String FRAGMENT_DEVICE_UNSUPPORTED = "device_unsupported";

    private SharedPreferences prefs;
    private AppSettingMonitor serviceSettingListener;
    private AllPermissionsMonitor allPermissionsMonitor;
    private AdbPermissionCheck adbPermissionCheck;

    private View statusSection;
    private View permissionsSection;
    private TextView appStatusTextView;

    @SuppressLint("UseSwitchCompatOrMaterialCode")
    private Switch onOffSwitch;
    private Button allowPermissionsButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = ApplicationSettings.getSharedPreferences(this);
        serviceSettingListener = new AppSettingMonitor(this, ApplicationSettings.KEY_ENABLED);
        allPermissionsMonitor = new AllPermissionsMonitor(this);
        adbPermissionCheck = new AdbPermissionCheck();
        adbPermissionCheck.onResult.subscribe(gotPermissionStatus);

        statusSection = findViewById(R.id.app_section_status);
        permissionsSection = findViewById(R.id.app_section_permissions);
        appStatusTextView = findViewById(R.id.app_status);
        onOffSwitch = findViewById(R.id.on_off_switch);
        allowPermissionsButton = findViewById(R.id.app_permissions_allow);

        allowPermissionsButton.setOnClickListener((v) -> startActivity(new Intent(this, PermissionWizardActivity.class)));
        onOffSwitch.setOnClickListener(v -> {
            boolean targetEnabledState = onOffSwitch.isChecked();
            prefs
                    .edit()
                    .putBoolean(ApplicationSettings.KEY_ENABLED, targetEnabledState)
                    .apply();
        });

        boolean isSupportedDevice = DeviceInfo.isNvidiaShieldTv() || BuildConfig.DEBUG && DeviceInfo.isEmulator();
        if (!isSupportedDevice) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment existingFragment = fragmentManager.findFragmentByTag(FRAGMENT_DEVICE_UNSUPPORTED);

            if (existingFragment == null) {
                fragmentManager
                        .beginTransaction()
                        .add(android.R.id.content, new DeviceUnsupportedFragment(), FRAGMENT_DEVICE_UNSUPPORTED)
                        .commit();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        allowPermissionsButton = null;
        onOffSwitch = null;
        appStatusTextView = null;
        permissionsSection = null;
        statusSection = null;

        adbPermissionCheck.onResult.unsubscribe(gotPermissionStatus);
        adbPermissionCheck = null;
        allPermissionsMonitor = null;
        serviceSettingListener = null;
        prefs = null;
    }

    @Override
    protected void onStart() {
        super.onStart();

        // This activity has just become visible, so we'll ensure the service is running if needed
        // in case it was killed by a crash or the user
        ServiceController.syncServiceState(this);

        // Ideally, we would kick off the permission check every time the activity is resumed,
        // but the problem is this will cause ADB dialog spam since the dialog will take focus away
        // from our activity and then return it when the dialog is closed
        adbPermissionCheck.ensureStarted();

        // Listening for changes to the service setting in case it changes from outside the app, eg.
        // from a crash or revoked permission
        serviceSettingListener.subscribe(serviceSettingChanged);
        syncServiceSettingToUi();
    }

    @Override
    protected void onStop() {
        super.onStop();
        serviceSettingListener.unsubscribe(serviceSettingChanged);
        adbPermissionCheck.ensureStopped();
    }

    private final EventHandler gotPermissionStatus = new EventHandler() {
        @Override
        public void update() {
            boolean hasAdbPermission = adbPermissionCheck.isPermanentlyAuthorised();
            adbPermissionCheck.ensureStopped();

            boolean hasAllPermissions = allPermissionsMonitor.hasPermissions() && hasAdbPermission;

            boolean statusWasVisible = statusSection.getVisibility() == View.VISIBLE;
            boolean permissionWasVisible = permissionsSection.getVisibility() == View.VISIBLE;
            boolean permissionsButtonHadFocus = allowPermissionsButton.hasFocus();
            boolean onOffSwitchHadFocus = onOffSwitch.hasFocus();
            statusSection.setVisibility(hasAllPermissions ? View.VISIBLE : View.GONE);
            permissionsSection.setVisibility(hasAllPermissions ? View.GONE : View.VISIBLE);

            if (hasAllPermissions) {
                if (!statusWasVisible && permissionsButtonHadFocus)
                    onOffSwitch.requestFocus();
            } else {
                if (!permissionWasVisible && onOffSwitchHadFocus)
                    allowPermissionsButton.requestFocus();
            }
        }
    };

    private final EventHandler serviceSettingChanged = this::syncServiceSettingToUi;

    private void syncServiceSettingToUi() {
        boolean isEnabled = prefs.getBoolean(ApplicationSettings.KEY_ENABLED, false);

        String appName = getString(R.string.app_name);
        String appStatus = getString(isEnabled ? R.string.app_status_on : R.string.app_status_off, appName);
        appStatusTextView.setText(appStatus);
        onOffSwitch.setChecked(isEnabled);
    }

}
