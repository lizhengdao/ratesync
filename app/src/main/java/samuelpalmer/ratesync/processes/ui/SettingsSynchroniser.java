package samuelpalmer.ratesync.processes.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

import samuelpalmer.ratesync.processes.service.SettingsReceiver;
import samuelpalmer.ratesync.utilities.State;

/**
 * Listens to changes to {@link ApplicationSettings} in the UI process and sends them to the service
 * process
 */
public class SettingsSynchroniser extends State {

    private static final String TAG = SettingsSynchroniser.class.getSimpleName();

    private final Context context;
    private final SharedPreferences prefs;

    public SettingsSynchroniser(Context context) {
        this.context = context;
        prefs = ApplicationSettings.getSharedPreferences(context);
    }

    @Override
    protected void start() {
        prefs.registerOnSharedPreferenceChangeListener(preferenceChanged);

        Log.d(TAG, "Synchronising app settings to service process");
        syncSettings();
    }

    @Override
    protected void stop() {
        prefs.unregisterOnSharedPreferenceChangeListener(preferenceChanged);
    }

    private final SharedPreferences.OnSharedPreferenceChangeListener preferenceChanged = (sharedPreferences, key) -> {
        if (!isStarted()) {
            Log.w(TAG, "Got shared preference change while not started");
            return;
        }

        if (key == null) {
            Log.d(TAG, "App settings cleared. Synchronising to service process.");
            syncSettings();
        } else if (isSyncableSetting(key)) {
            Log.d(TAG, String.format("App setting %s changed. Synchronising to service process.", key));
            syncSettings();
        }
    };

    /**
     * Determines whether to synchonise the given {@link ApplicationSettings} setting to the service
     * process.
     *
     * @param key The key from {@link ApplicationSettings} or {@link samuelpalmer.ratesync.processes.service.ServiceSettings} to check
     */
    @SuppressWarnings({"SameReturnValue", "unused", "RedundantSuppression"})
    public static boolean isSyncableSetting(@NonNull String key) {
        // Add checks here for any settings that you want to make available to the service process
        return false;
    }

    private void syncSettings() {
        Map<String, ?> appSettings =  prefs.getAll();

        HashMap<String, Object> settingsToSync = new HashMap<>();
        for (String key : appSettings.keySet()) {
            if (isSyncableSetting(key)) {
                Object value = appSettings.get(key);
                settingsToSync.put(key, value);
            }
        }

        SettingsReceiver.sendSettings(context, settingsToSync);
    }

}
