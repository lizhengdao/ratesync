package samuelpalmer.ratesync.processes.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Starts the application process after this app is upgraded or the device is started up. This is
 * needed to ensure the service restarts after an upgrade or device reboot.
 */
public class StartServiceReceiver extends BroadcastReceiver {

    private static final String TAG = StartServiceReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action == null || !action.equals(Intent.ACTION_MY_PACKAGE_REPLACED) && !action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            Log.w(TAG, "Received unexpected action: " + action + ". Ignoring.");
            return;
        }

        Log.i(TAG, "Received intent " + action);
        // No need to do anything since the UI process's start-up logic will have already updated the service running state
    }

}
