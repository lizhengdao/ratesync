package samuelpalmer.ratesync.processes.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import samuelpalmer.ratesync.utilities.eventhandling.EventRegistrarAdaptor;

/**
 * Listens for and reports changes to a particular application setting in real-time
 */
public class AppSettingMonitor extends EventRegistrarAdaptor {

    private static final String TAG = AppSettingMonitor.class.getSimpleName();

    private final SharedPreferences prefs;
    private final String targetKey;

    /**
     * @param key The key of the app setting to monitor
     */
    public AppSettingMonitor(Context context, String key) {
        if (key == null)
            throw new IllegalArgumentException("Key is null");

        prefs = ApplicationSettings.getSharedPreferences(context);
        this.targetKey = key;
    }

    @Override
    protected void subscribeToAdaptee() {
        prefs.registerOnSharedPreferenceChangeListener(prefsChanged);
    }

    @Override
    protected void unsubscribeFromAdaptee() {
        prefs.unregisterOnSharedPreferenceChangeListener(prefsChanged);
    }

    private final SharedPreferences.OnSharedPreferenceChangeListener prefsChanged = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (!hasSubscriptions()) {
                Log.w(TAG, "Received setting change while not subscribed. Ignoring.");
                return;
            }

            boolean isOnSetting = targetKey.equals(key);
            if (!isOnSetting) {
                // A different setting was changed. Not related to what we're doing here.
                return;
            }

            Log.i(TAG, "Setting " + key + " changed. Reporting.");
            report();
        }
    };

}
