package samuelpalmer.ratesync.processes.ui;

import android.content.Context;
import android.util.Log;

import samuelpalmer.ratesync.utilities.State;
import samuelpalmer.ratesync.utilities.eventhandling.EventHandler;

/**
 * Synchronises the {@link ApplicationSettings#KEY_ENABLED} setting
 * to the service running state. This allows us to indirectly control the service running state
 * by changing the setting, while persisting the user's preferred running state across device
 * restarts, crashes, etc.
 */
public class ServiceStateSynchroniser extends State {

    private static final String TAG = ServiceStateSynchroniser.class.getSimpleName();

    private final AppSettingMonitor appSettingMonitor;
    private final Context context;

    public ServiceStateSynchroniser(Context context) {
        appSettingMonitor = new AppSettingMonitor(context, ApplicationSettings.KEY_ENABLED);
        this.context = context;
    }

    @Override
    protected void start() {
        appSettingMonitor.subscribe(onStateChanged);
        Log.i(TAG, "Starting up. Syncing.");
        ServiceController.syncServiceState(context);
    }

    @Override
    protected void stop() {
        appSettingMonitor.unsubscribe(onStateChanged);
    }

    private final EventHandler onStateChanged = new EventHandler() {
        @Override
        public void update() {
            Log.i(TAG, "Service setting changed. Syncing.");
            ServiceController.syncServiceState(context);
        }
    };

}
