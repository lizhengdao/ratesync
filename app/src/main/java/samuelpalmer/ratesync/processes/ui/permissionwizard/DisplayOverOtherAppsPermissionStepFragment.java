package samuelpalmer.ratesync.processes.ui.permissionwizard;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.leanback.app.GuidedStepSupportFragment;

import samuelpalmer.ratesync.R;
import samuelpalmer.ratesync.processes.service.RefreshRateOverrider;
import samuelpalmer.ratesync.utilities.AppOpMonitor;
import samuelpalmer.ratesync.utilities.eventhandling.EventRegistrar;

public class DisplayOverOtherAppsPermissionStepFragment extends PermissionStepFragment {

    private AppOpMonitor permissionMonitor;

    @Override
    protected int getTitleResourceId() {
        return R.string.permission_display_over_other_apps_title;
    }

    @Override
    protected int getExplanationResourceId() {
        return R.string.permission_display_over_other_apps_explanation;
    }

    @Override
    protected EventRegistrar onPermissionChanged() {
        return permissionMonitor;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        permissionMonitor = RefreshRateOverrider.makePermissionMonitor(context);
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        permissionMonitor = null;
    }

    @Override
    protected boolean hasPermission() {
        return permissionMonitor.hasPermission();
    }

    @Override
    protected Intent getPromptIntent() {
        return RefreshRateOverrider.makeAcquirePermissionIntent();
    }

    @Override
    protected CharSequence getInstructions() {
        return getString(R.string.permission_display_over_other_apps_instructions, getString(R.string.app_name));
    }

    @Override
    protected GuidedStepSupportFragment getNextStep() {
        return new DebuggingPermissionStepFragment();
    }

}
