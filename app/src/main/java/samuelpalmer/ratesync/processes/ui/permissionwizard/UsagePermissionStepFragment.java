package samuelpalmer.ratesync.processes.ui.permissionwizard;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.leanback.app.GuidedStepSupportFragment;

import samuelpalmer.ratesync.processes.service.ActivityMonitor;
import samuelpalmer.ratesync.R;
import samuelpalmer.ratesync.utilities.AppOpMonitor;
import samuelpalmer.ratesync.utilities.eventhandling.EventRegistrar;

public class UsagePermissionStepFragment extends PermissionStepFragment {

    private AppOpMonitor permissionMonitor;

    @Override
    protected int getTitleResourceId() {
        return R.string.permission_usage_title;
    }

    @Override
    protected int getExplanationResourceId() {
        return R.string.permission_usage_explanation;
    }

    @Override
    protected EventRegistrar onPermissionChanged() {
        return permissionMonitor;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        permissionMonitor = ActivityMonitor.makePermissionMonitor(context);
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        permissionMonitor = null;
    }

    @Override
    protected boolean hasPermission() {
        return permissionMonitor.hasPermission();
    }

    @Override
    protected Intent getPromptIntent() {
        return ActivityMonitor.makeAcquirePermissionIntent();
    }

    @Override
    protected CharSequence getInstructions() {
        return getString(R.string.permission_usage_instructions, getString(R.string.app_name));
    }

    @Override
    protected GuidedStepSupportFragment getNextStep() {
        return new NotificationPermissionStepFragment();
    }

}
