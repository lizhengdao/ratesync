package samuelpalmer.ratesync.processes.service;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import samuelpalmer.ratesync.logcat.LogcatReader;
import samuelpalmer.ratesync.utilities.eventhandling.EventRegistrarAdaptor;

/**
 * Monitors NVIDIA Shield device logs to detect the frame rate of currently-playing videos.
 */
public class FrameRateMonitor extends EventRegistrarAdaptor {

    private static final String TAG = FrameRateMonitor.class.getSimpleName();

    private final Context context;

    /** Pattern of the log line that is output when a video first starts playing */
    private static final Pattern startOfVideoFrameRatePattern = Pattern.compile("\\bFrameRate\\b = (\\d+)\\b");

    @Nullable
    private LogcatReader logcatReader;

    @Nullable
    private Float frameRate;

    public FrameRateMonitor(Context context) {
        this.context = context;
    }

    @Override
    protected void subscribeToAdaptee() {
        if (logcatReader != null)
            throw new IllegalStateException("Already started");

        // Checking the clock straight away so we don't miss any log entries in between the start of this method and the ADB call
        Date startTime = new Date();

        LogcatReader.Args logcatArgs = new LogcatReader.Args();
        logcatArgs.buffer = "main";
        logcatArgs.format = "raw";
        logcatArgs.filter = startOfVideoFrameRatePattern;
        logcatArgs.since = startTime;

        logcatReader = LogcatReader.start(context, logcatArgs, this::onLogLine);
    }

    @Override
    protected void unsubscribeFromAdaptee() {
        if (logcatReader == null)
            throw new IllegalStateException("Already stopped");

        logcatReader.stop();
        logcatReader = null;
    }

    private void onLogLine(String line) {
        Matcher matcher = FrameRateMonitor.startOfVideoFrameRatePattern.matcher(line);
        if (!matcher.find())
            throw new RuntimeException("Didn't find framerate in log line " + line + " using pattern " + FrameRateMonitor.startOfVideoFrameRatePattern);

        String group = matcher.group(1);
        assert group != null;

        int nvidiaLogStartupFrameRate = Integer.parseInt(group);
        float newFrameRate = FrameRate.fromNvidiaLogStartUp(nvidiaLogStartupFrameRate);
        Log.i(TAG, "Frame rate received: " + nvidiaLogStartupFrameRate + " FPS. Mapped to: " + newFrameRate + " FPS.");

        // Always reporting frame rate updates, even if the new frame rate is the same as the last one.
        // This is because the user might have switched from one video to another that has the same frame rate,
        // so we still need to wait for the frame rate update to arrive.
        this.frameRate = newFrameRate;
        report();
    }

    @Nullable
    public Float getFrameRate() {
        if (!super.hasSubscriptions())
            throw new IllegalStateException("Cannot get frame rate because no subscribers are attached");

        return frameRate;
    }

}
