package samuelpalmer.ratesync.processes.service;

import java.util.Comparator;

/**
 * Sorts refresh rates by how compatible they are with the given frame rate. More compatible refresh
 * rates will be considered "less" (aka earlier) than less compatible refresh rates.
 */
public class FrameRateToRefreshRateComparator implements Comparator<Float> {

    private final float frameRateFps;

    public FrameRateToRefreshRateComparator(float frameRateFps) {
        this.frameRateFps = frameRateFps;
    }

    @SuppressWarnings("RedundantSuppression")
    @Override
    public int compare(Float refreshRateHz1, Float refreshRateHz2) {
        float frameDropsPerSecond1 = frameRateFps - refreshRateHz1;
        float frameDropsPerSecond2 = frameRateFps - refreshRateHz2;
        boolean hasFrameDrops1 = frameDropsPerSecond1 > 0;
        boolean hasFrameDrops2 = frameDropsPerSecond2 > 0;

        if (hasFrameDrops1 != hasFrameDrops2) {
            // Our first preference is to use a refresh rate that doesn't drop any frames
            return Boolean.compare(hasFrameDrops1, hasFrameDrops2);
        }

        //noinspection ConstantConditions
        if (hasFrameDrops1 && hasFrameDrops2) {
            // If we must drop frames, then we'll at least pick the refresh rate that drops the least amount
            return Float.compare(frameDropsPerSecond1, frameDropsPerSecond2);
        }

        float judder1 = calculateJudderProportion(frameRateFps, refreshRateHz1);
        float judder2 = calculateJudderProportion(frameRateFps, refreshRateHz2);
        if (judder1 != judder2) {
            // We prefer the one with the least judder
            return Float.compare(judder1, judder2);
        }

        // The refresh rates have no frame drops and equal judder, so we'll pick the smaller one
        // since that increases the chances of the display supporting advanced features like HDR
        // and motion interpolation
        return Float.compare(refreshRateHz1, refreshRateHz2);
    }

    /**
     * Calculates the proportion of time that frames will be being displayed for too long or too
     * short a time.
     */
    private static float calculateJudderProportion(float frameRateFps, float refreshRateHz) {
        float refreshCyclesPerFrame = refreshRateHz / frameRateFps;
        float leftoverRefreshCyclesPerFrame = refreshCyclesPerFrame - (int)refreshCyclesPerFrame;
        float judderRefreshCyclesPerFrame = leftoverRefreshCyclesPerFrame > 0.5 ? 1 - leftoverRefreshCyclesPerFrame : leftoverRefreshCyclesPerFrame;
        float judderRefreshCyclesPerSecond = judderRefreshCyclesPerFrame * frameRateFps;
        return judderRefreshCyclesPerSecond / refreshRateHz;
    }

}
