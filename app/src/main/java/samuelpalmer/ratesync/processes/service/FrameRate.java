package samuelpalmer.ratesync.processes.service;

import android.util.Log;

import androidx.annotation.NonNull;

import java.text.NumberFormat;

public class FrameRate {

    private static final String TAG = FrameRate.class.getSimpleName();

    // Don't re-order these since it will affect the mapping logic further down in this class
    private static final FrameRate[] all = new FrameRate[] {
            new FrameRate(23.976f, 24),
            new FrameRate(24.000f, 24),
            new FrameRate(25.000f, 25),
            new FrameRate(29.970f, 30),
            new FrameRate(30.000f, 30),
            new FrameRate(50.000f, 50),
            new FrameRate(59.940f, 62),
            new FrameRate(60.000f, 62),
    };

    /**
     * Guesses the video frame rate from the NVIDIA Shield TV's log message that is output when a
     * video first plays. The frame rate from the log is always an integer, and may be off by up to
     * 3 FPS, which is why we have to guess. When guessing, we err on the side of a lower frame rate
     * (eg 23.976 instead of 24.000) since these seem to be more commonly used.
     *
     * Frame rates over 60 FPS are unsupported since I don't have a display that supports those
     * modes to test on, so the passed-in frame rate will be returned unchanged.
     */
    public static float fromNvidiaLogStartUp(int nvidiaLogStartUp) {
        for (FrameRate frameRate : all)
            if (frameRate.nvidiaLogStartUp == nvidiaLogStartUp)
                return frameRate.actual;

        Log.w(TAG, "No mapping found for NVIDIA Shield TV start-up frame rate " + nvidiaLogStartUp + " FPS. Mapping to original value, which might be incorrect.");
        return nvidiaLogStartUp;
    }

    public final float actual;
    public final int nvidiaLogStartUp;

    private FrameRate(float actual, int nvidiaLogStartUp) {
        this.actual = actual;
        this.nvidiaLogStartUp = nvidiaLogStartUp;
    }

    @Override
    @NonNull
    public String toString() {
        return NumberFormat.getInstance().format(actual) + " FPS";
    }

}
