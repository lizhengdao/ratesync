package samuelpalmer.ratesync.processes.service;

import android.annotation.SuppressLint;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import samuelpalmer.ratesync.utilities.AppOpMonitor;
import samuelpalmer.ratesync.utilities.ObjectExtensions;
import samuelpalmer.ratesync.utilities.State;

/**
 * Suggests to the OS to change the display refresh rate.
 * Requires the {@link android.Manifest.permission#SYSTEM_ALERT_WINDOW} permission.
 */
@SuppressLint("ObsoleteSdkInt") // This can be removed once we lower the min SDK version
public class RefreshRateOverrider extends State {

    private static final String TAG = RefreshRateOverrider.class.getSimpleName();

    private final View view;
    private final WindowManager windows;
    private final WindowManager.LayoutParams layout;

    private Float currentFrameRateFps;

    public RefreshRateOverrider(Context context) {
        windows = ContextCompat.getSystemService(context, WindowManager.class);
        view = new View(context);
        layout = generateLayout();
    }

    public static Intent makeAcquirePermissionIntent() {
        // The OS overlay permission intent does not work as of Android 9.0. So instead, we need to direct the user to Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS.
        return new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
    }

    public static AppOpMonitor makePermissionMonitor(Context context) {
        return new AppOpMonitor(context, AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW);
    }

    @Override
    protected void start() {
        currentFrameRateFps = null;
    }

    @Override
    protected void stop() {
        if (currentFrameRateFps != null)
            windows.removeView(view);
    }

    private static WindowManager.LayoutParams generateLayout() {
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

        if (Build.VERSION.SDK_INT >= 26)
            layoutParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        else
            // This seems like the most relevant window type for what we're doing.
            // Note that TYPE_TOAST can't be used to bypass the permission requirement since it still needs the permission in MIUI V8
            //noinspection deprecation
            layoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY;

        // Just in case the window type somehow doesn't enforce this
        layoutParams.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE;

        // Prevents breaking apps that detect overlying windows
        // (eg UBank app, or enabling an accessibility service)
        layoutParams.width = 0;
        layoutParams.height = 0;

        // Try to make it completely invisible
        layoutParams.format = PixelFormat.TRANSPARENT;
        layoutParams.alpha = 0f;

        return layoutParams;
    }

    /**
     * Suggests to the OS to switch the display refresh rate to the most suitable refresh rate for
     * the given video frame rate
     */
    public void updateRefreshRateForFrameRate(Float newFrameRateFps) {
        verifyStarted();

        if (!ObjectExtensions.areEqual(newFrameRateFps, currentFrameRateFps)) {
            /*
             * This seems to be the quickest way of toggling enforcement. Changing the view visibility is
             * slower, and removing and adding the view is even slower again.
             *
             * There are a few ways of toggling the overlay:
             * 1. Fastest: Toggle its {@link LayoutParams#preferredRefreshRate} value. This causes the Android 6 "Screen Overlay Detected" problem even when enforcement is disabled.
             * 2. Medium: Toggle the view's visibility. This doesn't work in Android R, since even "INVISIBLE" or "GONE" windows still affect things.
             * 3. Slowest: Add/remove the view from the window manager.
             *
             * Picking the last option since it's the fastest one that doesn't have undesired side effects.
             */
            if (newFrameRateFps == null) {
                windows.removeView(view);
                Log.i(TAG, "Restored default refresh rate");
            } else {
                float actualRefreshRate = applyFrameRateToLayout(newFrameRateFps);

                if (currentFrameRateFps == null) {
                    try {
                        windows.addView(view, layout);
                    } catch (WindowManager.BadTokenException e) {
                        if (e.getMessage().contains("permission denied")) {
                            // The user probably revoked the permission while the service was running.
                            // We can ignore the error because it will be handled at a higher level
                            // in the app.
                            Log.w(TAG, "Missing permission to display over other apps. Ignoring.", e);
                        } else
                            throw e;
                    }
                } else {
                    windows.updateViewLayout(view, layout);
                }

                Log.i(TAG, "Set screen refresh rate to " +  actualRefreshRate + " Hz");
            }

            currentFrameRateFps = newFrameRateFps;
        }
    }

    @SuppressWarnings("RedundantSuppression")
    private float applyFrameRateToLayout(float frameRateFps) {
        // Checking the current display mode and available modes every time since both of these things could potentially have changed
        //noinspection deprecation
        Display display = windows.getDefaultDisplay();

        if (Build.VERSION.SDK_INT >= 23) {
            Display.Mode currentMode = display.getMode();
            Display.Mode[] supportedModes = display.getSupportedModes();

            List<Display.Mode> supportedModesInCurrentResolution = new ArrayList<>();

            // Limiting to modes that preserve the current screen resolution
            for (Display.Mode supportedMode : supportedModes)
                if (currentMode.getPhysicalWidth() == supportedMode.getPhysicalWidth() && currentMode.getPhysicalHeight() == supportedMode.getPhysicalHeight())
                    supportedModesInCurrentResolution.add(supportedMode);

            // Working out which is the most compatible
            Display.Mode bestMode = Collections.min(supportedModesInCurrentResolution, new FrameRateToModeComparator(frameRateFps));

            layout.preferredDisplayModeId = bestMode.getModeId();
            return bestMode.getRefreshRate();
        } else {
            //noinspection deprecation
            float[] supportedRefreshRates = display.getSupportedRefreshRates();

            List<Float> supportedRefreshRatesList = new ArrayList<>();
            for (float supportedRefreshRate : supportedRefreshRates)
                supportedRefreshRatesList.add(supportedRefreshRate);

            Float bestRefreshRate = Collections.min(supportedRefreshRatesList, new FrameRateToRefreshRateComparator(frameRateFps));
            //noinspection deprecation
            layout.preferredRefreshRate = bestRefreshRate;
            return bestRefreshRate;
        }
    }
}
