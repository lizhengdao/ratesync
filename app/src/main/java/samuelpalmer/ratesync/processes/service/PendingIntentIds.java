package samuelpalmer.ratesync.processes.service;

import android.app.PendingIntent;

import samuelpalmer.ratesync.processes.ui.RefreshRateSwitcherActivity;

public class PendingIntentIds {

    /** Automatic launch of {@link RefreshRateSwitcherActivity} from service */
    public static final int REFRESH_RATE_SWITCH = 1;

    // Always cancelling the existing pending intent since it's possible for the existing one to die and become permanently useless.
    public static final int PENDING_INTENT_FLAGS = PendingIntent.FLAG_CANCEL_CURRENT;

    private PendingIntentIds() {}

}
