package samuelpalmer.ratesync.processes.service;

import com.tananaev.adblib.AdbConnection;
import com.tananaev.adblib.AdbStream;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import samuelpalmer.ratesync.adb.AdbBackgroundConnection;

// TODO: Convert this class to instead open a single shared shell session, and then write lines to it as needed
public class ShellCommandRunner {

    /**
     * Runs the given shell command through ADB. Returns all output text from the command.
     */
    public static String run(String shellCommand) {
        AtomicReference<String> commandOutput = new AtomicReference<>();
        CountDownLatch latch = new CountDownLatch(1);

        AdbBackgroundConnection backgroundConnection = new AdbBackgroundConnection(new AdbBackgroundConnection.ConnectionCallbacks() {
            @Override
            public void onConnected(AdbConnection connection) {
                // Send ADB command
                try (AdbStream stream = connection.open("shell:" + shellCommand)) {
                    // Start processing response data
                    StringBuilder buffer = new StringBuilder();

                    while (!stream.isClosed()) {
                        // Reading the next chunk of data
                        byte[] chunkBytes;
                        try {
                            chunkBytes = stream.read();
                        } catch (IOException e) {
                            // The stream has closed, so we're all done
                            break;
                        }

                        // Decoding chunk
                        String chunkText = new String(chunkBytes, StandardCharsets.UTF_8);
                        buffer.append(chunkText);
                    }

                    commandOutput.set(buffer.toString().trim());
                } catch (IOException e) {
                    throw new RuntimeException("ADB comms error", e);
                } catch (InterruptedException e) {
                    throw new RuntimeException("Unexpectedly interrupted while running shell command", e);
                }

                latch.countDown();
            }

            @Override
            public void onPermanentAuthMissing() {
                throw new RuntimeException("We should never get here since we're doing all the work synchronously");
            }

            @Override
            public void onNetworkDebuggingDisabled() {
                // TODO: Do we need to instead get here on the worker thread so we can still handle it?
                throw new RuntimeException("We should never get here since we're doing all the work synchronously");
            }
        });
        backgroundConnection.ensureStarted();

        try {
            boolean finished = latch.await(10, TimeUnit.SECONDS);
            if (!finished)
                throw new RuntimeException("Shell command took too long");

            backgroundConnection.ensureStopped();
        } catch (InterruptedException e) {
            throw new RuntimeException("Unexpectedly interrupted while waiting for shell command", e);
        }

        return commandOutput.get();
    }

    private ShellCommandRunner() {}

}
