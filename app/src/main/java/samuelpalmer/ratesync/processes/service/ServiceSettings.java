package samuelpalmer.ratesync.processes.service;

import android.content.Context;
import android.content.SharedPreferences;

import samuelpalmer.ratesync.CurrentProcess;

/**
 * Holds persistent service state. Not intended for backup or sync.
 * Only for use within the service process.
 *
 * This is intentionally kept simple so that {@link SharedPreferences} access is consistent with
 * other apps.
 */
public abstract class ServiceSettings {

    public static SharedPreferences getSharedPreferences(Context context) {
        if (!CurrentProcess.isServiceProcess(context))
            throw new RuntimeException("Service settings accessed outside of the service process");

        return context.getSharedPreferences("service", Context.MODE_PRIVATE);
    }

    // NOTE: If you change preference config, add a migration to the "Migrations" class

    /**
     * Used for upgrading settings data when upgrading to a newer version of the app.
     */
    public static final String KEY_SETTINGS_VERSION = "version";

}
