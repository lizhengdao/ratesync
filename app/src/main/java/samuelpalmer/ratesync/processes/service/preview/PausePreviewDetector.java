package samuelpalmer.ratesync.processes.service.preview;

import android.content.Context;
import android.media.session.MediaController;
import android.media.session.PlaybackState;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.os.HandlerCompat;

import samuelpalmer.ratesync.processes.service.KeyEventListener;
import samuelpalmer.ratesync.utilities.FieldConstants;
import samuelpalmer.ratesync.utilities.eventhandling.EventHandler;

/**
 * Detects previews by attempting to pause playback. If the pause succeeds, then the video is not
 * a preview. Otherwise it is.
 */
@SuppressWarnings({"unused", "RedundantSuppression"})
class PausePreviewDetector extends PreviewDetector {

    private static final String TAG = PausePreviewDetector.class.getSimpleName();

    private static final FieldConstants<PlaybackState> playbackStates = new FieldConstants<>(PlaybackState.class, "STATE_");

    /** How long to wait between pause attempts, in milliseconds */
    private static final int PAUSE_ATTEMPT_INTERVAL_MILLIS = 500;

    /** How long the video must stay in paused state for us to confirm it's not just transitioning into stopped state */
    private static final int PAUSE_HOLD_TIME_MILLIS = 500;

    /** How long to postpone pause attempts after the user presses a key, in milliseconds */
    private static final int KEY_PRESS_DELAY_MILLIS = 1000;

    private final MediaController mediaSession;
    private final Handler handler;
    private final KeyEventListener keyEventListener;

    private DetectionState detectionState;
    private boolean isPossiblyPreview;

    private enum DetectionState {
        /** We are not currently doing detection */
        NOT_STARTED,

        /** Video may not be playing, so are waiting for it to start so we can check whether we can pause it */
        WAITING_FOR_PLAYBACK,

        /** Video is playing, and we are actively attempting to pause it to see if it responds */
        ATTEMPTING_TO_PAUSE,

        /** Postponing detection because the user recently pressed a button so may have interacted with playback */
        DETECTION_POSTPONED,

        /** Video appears to have been paused by us, so we are waiting to confirm it isn't just transitioning into stopped state */
        HOLDING_PAUSE,

        /** We have confirmed the video is not a preview, so we no longer need to do anything */
        DETECTION_COMPLETE,
    }

    public PausePreviewDetector(Context context, MediaController mediaSession) {
        this.mediaSession = mediaSession;
        this.detectionState = DetectionState.NOT_STARTED;
        handler = HandlerCompat.createAsync(Looper.getMainLooper());
        keyEventListener = new KeyEventListener(context);
    }

    @Override
    public boolean isPossiblyPreview() {
        if (!hasSubscriptions())
            throw new IllegalStateException("Not subscribed");

        return isPossiblyPreview;
    }

    @Override
    protected void subscribeToAdaptee() {
        detectionState = DetectionState.WAITING_FOR_PLAYBACK;
        isPossiblyPreview = true;

        keyEventListener.subscribe(onKeyPress);
        mediaSession.registerCallback(sessionStateChanged, handler);

        PlaybackState playbackState = mediaSession.getPlaybackState();
        if (playbackState != null)
            handlePlaybackState(playbackState);
    }

    @Override
    protected void unsubscribeFromAdaptee() {
        detectionState = DetectionState.NOT_STARTED;
        clearCallbacks();

        mediaSession.unregisterCallback(sessionStateChanged);
        keyEventListener.unsubscribe(onKeyPress);
    }

    private final EventHandler onKeyPress = new EventHandler() {

        @Override
        public void update() {
            if (detectionState == DetectionState.NOT_STARTED) {
                Log.w(TAG, "Got key press event while not started");
                return;
            }

            if (detectionState == DetectionState.DETECTION_COMPLETE) {
                // We've finished our work, so key-presses are inconsequential for us now
                return;
            }

            // The user has pressed a button, which could influence media playback, so we no longer
            // know if playback is responding to us or to the user. For example, if the user opens
            // the episodes list then the preview pauses.
            Log.i(TAG, "Got key press. Postponing detection.");
            detectionState = DetectionState.DETECTION_POSTPONED;
            clearCallbacks();
            handler.postDelayed(resumeDetection, KEY_PRESS_DELAY_MILLIS);
        }

    };

    private void clearCallbacks() {
        handler.removeCallbacks(resumeDetection);
        handler.removeCallbacks(attemptPause);
        handler.removeCallbacks(pauseHoldComplete);
    }

    private final MediaController.Callback sessionStateChanged = new MediaController.Callback() {

        @Override
        public void onPlaybackStateChanged(@Nullable PlaybackState playbackState) {
            super.onPlaybackStateChanged(playbackState);

            if (playbackState == null) {
                Log.w(TAG, "Playback state changed to null. Ignoring.");
                return;
            }

            handlePlaybackState(playbackState);
        }

    };

    private void handlePlaybackState(@NonNull PlaybackState playbackState) {
        int playState = playbackState.getState();

        switch (detectionState) {
            case NOT_STARTED:
                Log.w(TAG, "Got playback state update while not started");
                break;
            case WAITING_FOR_PLAYBACK:
                if (playState == PlaybackState.STATE_PLAYING) {
                    Log.i(TAG, "Video is playing. Attempting to pause to work out if it's a preview.");
                    detectionState = DetectionState.ATTEMPTING_TO_PAUSE;
                    attemptPause.run();
                }

                break;
            case ATTEMPTING_TO_PAUSE:
                if (playState == PlaybackState.STATE_PAUSED) {
                    Log.i(TAG, "Playback paused. Waiting to confirm this isn't a preview transitioning into stopped state.");
                    handler.removeCallbacks(attemptPause);
                    detectionState = DetectionState.HOLDING_PAUSE;
                    handler.postDelayed(pauseHoldComplete, PAUSE_HOLD_TIME_MILLIS);
                } else if (playState != PlaybackState.STATE_PLAYING) {
                    Log.i(TAG, "Playback transitioned into " + playbackStates.getName(playState) + ". Resetting.");
                    handler.removeCallbacks(attemptPause);
                    detectionState = DetectionState.WAITING_FOR_PLAYBACK;
                }

                break;
            case HOLDING_PAUSE:
                if (playState != PlaybackState.STATE_PAUSED) {
                    Log.i(TAG, "Playback transitioned into " + playbackStates.getName(playState) + ". Maybe the pause was triggered by a preview completing. Resetting.");
                    handler.removeCallbacks(pauseHoldComplete);
                    detectionState = DetectionState.WAITING_FOR_PLAYBACK;
                }

                break;
        }
    }

    private final Runnable attemptPause = new Runnable() {
        @Override
        public void run() {
            if (detectionState != DetectionState.ATTEMPTING_TO_PAUSE)
                throw new RuntimeException("Got pause attempt callback, but we're in state " + detectionState.name());

            Log.v(TAG, "Attempting pause");
            mediaSession.getTransportControls().pause();
            handler.postDelayed(attemptPause, PAUSE_ATTEMPT_INTERVAL_MILLIS);
        }
    };

    private final Runnable resumeDetection = new Runnable() {
        @Override
        public void run() {
            if (detectionState != DetectionState.DETECTION_POSTPONED)
                throw new RuntimeException("Got resume pause attempts callback, but we're in state " + detectionState.name());

            Log.i(TAG, "Resuming detection");
            detectionState = DetectionState.WAITING_FOR_PLAYBACK;
            PlaybackState playbackState = mediaSession.getPlaybackState();

            if (playbackState != null)
                handlePlaybackState(playbackState);
        }
    };

    private final Runnable pauseHoldComplete = new Runnable() {
        @Override
        public void run() {
            if (detectionState != DetectionState.HOLDING_PAUSE)
                throw new RuntimeException("Got pause hold complete callback while, but we're in state " + detectionState.name());

            Log.i(TAG, "Pause held for " + PAUSE_HOLD_TIME_MILLIS + "ms. Playback is not a preview.");

            // Leaving the video paused since we're about to switch out of the app to change the display mode
            detectionState = DetectionState.DETECTION_COMPLETE;
            isPossiblyPreview = false;
            report();
        }
    };

}
