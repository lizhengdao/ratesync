package samuelpalmer.ratesync.processes.service.preview;

/**
 * Assumes that every video is not a preview. Used for apps that either don't show previews or do
 * show them but without using media sessions.
 */
@SuppressWarnings({"unused", "RedundantSuppression"})
class NullPreviewDetector extends PreviewDetector {

    @Override
    public boolean isPossiblyPreview() {
        return false;
    }

    @Override
    protected void subscribeToAdaptee() {}

    @Override
    protected void unsubscribeFromAdaptee() {}

}
