package samuelpalmer.ratesync.processes.service;

import android.annotation.SuppressLint;
import android.app.AppOpsManager;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.os.HandlerCompat;

import samuelpalmer.ratesync.BuildConfig;
import samuelpalmer.ratesync.utilities.AppOpMonitor;
import samuelpalmer.ratesync.utilities.ObjectExtensions;
import samuelpalmer.ratesync.utilities.eventhandling.EventRegistrarAdaptor;

@SuppressWarnings("RedundantSuppression")
public class ActivityMonitor extends EventRegistrarAdaptor {

    private static final String TAG = ActivityMonitor.class.getSimpleName();
    private static final int POLL_INTERVAL_MILLIS = 500;
    private static final int ACTIVITY_IN_FOREGROUND;

    static {
        if (Build.VERSION.SDK_INT >= 29)
            ACTIVITY_IN_FOREGROUND = UsageEvents.Event.ACTIVITY_RESUMED;
        else
            //noinspection deprecation
            ACTIVITY_IN_FOREGROUND = UsageEvents.Event.MOVE_TO_FOREGROUND;
    }

    @SuppressWarnings("unused")
    @SuppressLint("ObsoleteSdkInt")
    public static boolean isSupported() {
        // The OS settings app doesn't include a UI to enable usage access until Android TV 8.1.0.
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1;
    }

    private final Handler handler;
    private final UsageStatsManager usageStatsManager;

    @Nullable
    private ComponentName lastActivity;

    private long lastActivityTime;

    public ActivityMonitor(Context context) {
        this.handler = HandlerCompat.createAsync(Looper.getMainLooper());

        // Not using ContextCompat.getSystemService since it returns null on API 21
        usageStatsManager = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
    }

    /**
     * @return The top-most activity or <code>null</code> if unknown.
     */
    @Nullable
    public ComponentName currentOrNull() {
        if (!hasSubscriptions())
            throw new IllegalStateException("Can't detect the activity when not started");

        return lastActivity;
    }

    public static Intent makeAcquirePermissionIntent() {
        // The OS usage permission intent does not work as of Android 9.0. So instead, we need to direct the user to Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS.
        return new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
    }

    public static AppOpMonitor makePermissionMonitor(Context context) {
        return new AppOpMonitor(context, AppOpsManager.OPSTR_GET_USAGE_STATS);
    }

    private ComponentName poll() {
        long nowMillis = System.currentTimeMillis();

        // Only looking back as far as the last known activity to minimise resource usage
        UsageEvents usageEvents = usageStatsManager.queryEvents(lastActivityTime > 0 ? lastActivityTime - 1 : 0, nowMillis);
        boolean gotEvents = false;
        long latestEventTime = -1;
        String latestEventPackage = null;
        String latestEventClassName = null;

        if (usageEvents != null) {
            UsageEvents.Event event = new UsageEvents.Event();

            while (usageEvents.hasNextEvent()) {
                gotEvents = true;

                // Apparently you need to always loop through all available events to avoid a crash: https://stackoverflow.com/a/50896301/238753
                usageEvents.getNextEvent(event);

                if (event.getEventType() == ACTIVITY_IN_FOREGROUND && event.getTimeStamp() > latestEventTime) {
                    // It looks safe to assume that MOVE_TO_FOREGROUND is for an activity: https://raw.githubusercontent.com/aosp-mirror/platform_frameworks_base/master/services/core/java/com/android/server/am/ActivityManagerService.java
                    latestEventPackage = event.getPackageName();
                    latestEventClassName = event.getClassName();
                    latestEventTime = event.getTimeStamp();
                }
            }
        }

        if (latestEventTime > lastActivityTime)
            lastActivityTime = latestEventTime;

        if (latestEventPackage != null) {
            String className = latestEventClassName;
            if (className == null) {
                Log.w(getClass().getSimpleName(), "Top activity has no class name" + (BuildConfig.DEBUG ? ": " + latestEventPackage : ""));
                className = ""; // This is to prevent ComponentName from complaining
            }

            return new ComponentName(latestEventPackage, className);
        }
        else {
            if (!gotEvents) {
                // This could mean that the usage permission is missing. That will be taken care of
                // at a higher level in the app code.

                if (lastActivity != null) {
                    // Only logging this if we previously knew the activity.
                    // This is to prevent the log message from repeating every time this is polled.

                    // Apparently some devices report that permission is granted when it's actually not, resulting in this situation.
                    // See https://stackoverflow.com/a/43583005/238753.
                    Log.w(getClass().getSimpleName(), "Didn't get any usage events. Usage permission is probably missing.");
                }

                return null; // We're in the dark about which app is on top since polling isn't working at the moment
            }
            else {
                // We got some data but couldn't find the last-opened activity. This shouldn't happen.
                // It's probably safe to assume that the last known activity is still the current one.
                return lastActivity;
            }
        }
    }

    @Override
    protected void subscribeToAdaptee() {
        pollAndSchedule(false); // Not sending an update since this is not a *change*, just the initial value
    }

    @Override
    protected void unsubscribeFromAdaptee() {
        lastActivity = null; // We no longer know if this is the current activity since we're now unsubscribed
        handler.removeCallbacks(poller);
    }

    private final Runnable poller = () -> {
        if (hasSubscriptions())
            pollAndSchedule(true);
    };

    private void pollAndSchedule(boolean sendUpdate) {
        ComponentName topActivity = poll();
        setTopActivity(topActivity, sendUpdate);
        handler.postDelayed(poller, POLL_INTERVAL_MILLIS);
    }

    private void setTopActivity(@Nullable ComponentName activityOrNull, boolean sendUpdate) {
        if (!ObjectExtensions.areEqual(activityOrNull, lastActivity)) {
            // Not logging specific activity in Production builds since that would be a privacy violation since logs are included in error reports.
            String logMessage = "Activity changed";
            if (BuildConfig.DEBUG)
                logMessage += " to " + (activityOrNull == null ? null : activityOrNull.flattenToShortString());
            Log.i(TAG, logMessage);

            lastActivity = activityOrNull;

            if (sendUpdate)
                report();
        }
    }

}
