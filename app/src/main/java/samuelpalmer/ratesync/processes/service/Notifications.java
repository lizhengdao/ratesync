package samuelpalmer.ratesync.processes.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.media.AudioAttributes;

import androidx.core.content.ContextCompat;

/**
 * Global configuration and metadata for all notifications raised by this app
 */
public class Notifications {

    /** Notification id of the permanent foreground service notification */
    public static final int ID_SERVICE = 1;

    /** Notification channel of the permanent foreground service notification */
    public static final String CHANNEL_SERVICE = "service";

    /**
     * Configures the mandatory notification channel for the permanent foreground service notification as required by Android.
     * Only making the one channel because Android TV doesn't display notifications, so we won't be
     * posting any others.
     */
    public static void createServiceChannel(Context context) {
        NotificationChannel channel = new NotificationChannel(CHANNEL_SERVICE, "Service", NotificationManager.IMPORTANCE_MIN);

        channel.setLockscreenVisibility(Notification.VISIBILITY_SECRET); // To reduce the chance of showing up anywhere
        channel.setShowBadge(false); // Since this is a permanent notification
        channel.setSound(null, new AudioAttributes.Builder().build()); // Just to make sure this never triggers a sound, even if the user configures it to do so, since the notification is just for a background service

        NotificationManager notificationManager = ContextCompat.getSystemService(context, NotificationManager.class);
        assert notificationManager != null;
        notificationManager.createNotificationChannel(channel);
    }

    private Notifications() {}

}
