package samuelpalmer.ratesync.processes.service;

import android.app.Notification;
import android.content.Intent;
import android.media.session.MediaSession;
import android.os.Bundle;
import android.provider.Settings;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

/**
 * This gives us permission to listen to media sessions from other apps. It also listens to
 * {@link Notification}s and logs info about them for use in development.
 */
public class NotificationListener extends NotificationListenerService {

    private static final String TAG = NotificationListener.class.getSimpleName();

    public static Intent makeAcquirePermissionIntent() {
        return new Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS);
    }

    @Override
    public void onListenerConnected() {
        super.onListenerConnected();

        Log.i(TAG, "Notification listener connected");
    }

    @Override
    public void onListenerDisconnected() {
        super.onListenerDisconnected();
        Log.i(TAG, "Notification listener disconnected");
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        super.onNotificationPosted(sbn);
        String key = sbn.getKey();
        String packageName = sbn.getPackageName();
        String category = sbn.getNotification().category;
        CharSequence tickerText = sbn.getNotification().tickerText;
        Bundle extras = sbn.getNotification().extras;
        String bigText = extras.getString(Notification.EXTRA_BIG_TEXT);
        String infoText = extras.getString(Notification.EXTRA_INFO_TEXT);
        MediaSession.Token mediaSession = extras.getParcelable(Notification.EXTRA_MEDIA_SESSION);
        String subText = extras.getString(Notification.EXTRA_SUB_TEXT);
        String text = extras.getString(Notification.EXTRA_TEXT);
        String title = extras.getString(Notification.EXTRA_TITLE);

        Log.i(TAG, "Notification posted: key = " + key +
                ", package = " + packageName +
                ", category = " + category +
                ", ticker text = " + tickerText +
                ", big text = " + bigText +
                ", info text = " + infoText +
                ", media session = " + mediaSession +
                ", sub text = " + subText +
                ", text = " + text +
                ", title = " + title);
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn, RankingMap rankingMap, int reason) {
        super.onNotificationRemoved(sbn, rankingMap, reason);

        Log.i(TAG, "Notification removed: key = " + sbn.getKey());
    }

}
