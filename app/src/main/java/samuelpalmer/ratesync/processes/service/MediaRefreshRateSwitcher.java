package samuelpalmer.ratesync.processes.service;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.session.MediaController;
import android.media.session.PlaybackState;
import android.util.Log;

import samuelpalmer.ratesync.processes.ui.RefreshRateSwitcherActivity;
import samuelpalmer.ratesync.utilities.FieldConstants;
import samuelpalmer.ratesync.utilities.eventhandling.EventHandler;
import samuelpalmer.ratesync.utilities.eventhandling.EventRegistrarAdaptor;

/**
 * Switches the refresh rate while a media session is active
 */
public class MediaRefreshRateSwitcher extends EventRegistrarAdaptor {

    private static final String TAG = MediaRefreshRateSwitcher.class.getSimpleName();

    private final Context context;
    private final PlaybackMonitor playbackMonitor;
    private final RefreshRateOverrider refreshRateOverrider;
    private final float targetFrameRateFps;
    private final ActivityMonitor activityMonitor;

    private State state;
    private MediaController currentSession;
    private String sessionPackage;

    public enum State {
        /** Not active */
        NOT_STARTED,

        /** Starting an activity to force the session to close to ensure it's safe to switch the refresh rate */
        STARTING_ACTIVITY,

        /** Overriding the refresh rate, but waiting for the display to come back on after the change */
        SWITCHING_REFRESH_RATE,

        /** The display's refresh rate has been set, and we're closing our activity and returning to the the media app */
        CLOSING_ACTIVITY,

        /** The refresh rate is fully set and playback is restored. Nothing left to do for this media session. */
        COMPLETE,
    }

    public MediaRefreshRateSwitcher(Context context, PlaybackMonitor playbackMonitor, RefreshRateOverrider refreshRateOverrider, float targetFrameRateFps) {
        this.context = context;
        this.playbackMonitor = playbackMonitor;
        this.refreshRateOverrider = refreshRateOverrider;
        this.targetFrameRateFps = targetFrameRateFps;
        activityMonitor = new ActivityMonitor(context);
    }

    public State getState() {
        return state;
    }

    @Override
    protected void subscribeToAdaptee() {
        currentSession = playbackMonitor.getCurrentSession();
        if (currentSession == null)
            throw new IllegalStateException("Not currently in a session. This is probably a bug.");

        sessionPackage = currentSession.getPackageName();

        playbackMonitor.subscribe(sessionChanged);
        activityMonitor.subscribe(activityChanged);
        context.registerReceiver(gotHdmiPlugEvent, new IntentFilter(AudioManager.ACTION_HDMI_AUDIO_PLUG));

        // We'll attempt to pause playback in case it takes a while for the activity to launch
        Log.d(TAG, "Sending pause command");
        currentSession.getTransportControls().pause();

        Log.i(TAG, "Starting activity");
        state = State.STARTING_ACTIVITY;
        startRefreshSwitcherActivity();
    }

    @Override
    protected void unsubscribeFromAdaptee() {
        reset();
    }

    private void reset() {
        if (state == State.NOT_STARTED)
            return;

        Log.i(TAG, "Stopping");

        if (state.compareTo(State.SWITCHING_REFRESH_RATE) >= 0 && state.compareTo(State.COMPLETE) < 0)
            refreshRateOverrider.updateRefreshRateForFrameRate(null);

        state = State.NOT_STARTED;

        RefreshRateSwitcherActivity.requestFinish(context);

        context.unregisterReceiver(gotHdmiPlugEvent);
        activityMonitor.unsubscribe(activityChanged);
        playbackMonitor.unsubscribe(sessionChanged);

        sessionPackage = null;
        currentSession = null;
    }

    private final EventHandler sessionChanged = new EventHandler() {
        @Override
        public void update() {
            MediaController newSession = playbackMonitor.getCurrentSession();

            switch (state) {
                case NOT_STARTED:
                    throw new IllegalStateException("Got session update while in " + state.name() + " state");
                case STARTING_ACTIVITY:
                    if (currentSession == null)
                        throw new IllegalStateException("In state " + state.name() + " but don't have a session");

                    if (newSession != null && newSession != currentSession) {
                        Log.w(TAG, "Got a new session while attempting to close the session. Aborting.");
                        reset();
                        report();
                    } else {
                        Log.i(TAG, "Session closed. Overriding display refresh rate for " + targetFrameRateFps + " FPS");
                        state = State.SWITCHING_REFRESH_RATE;
                        currentSession = null;
                        refreshRateOverrider.updateRefreshRateForFrameRate(targetFrameRateFps);
                    }

                    break;
                case SWITCHING_REFRESH_RATE:
                    if (currentSession != null)
                        throw new IllegalStateException("In state " + state.name() + " but we have a session");

                    if (newSession != null) {
                        Log.w(TAG, "Received new media session in the middle of the refresh rate switch. Resetting.");
                        reset();
                        report();
                    }

                    break;
                case CLOSING_ACTIVITY:
                    if (currentSession != null)
                        throw new IllegalStateException("In state " + state.name() + " but we already have a session");

                    if (newSession != null) {
                        if (!newSession.getPackageName().equals(sessionPackage)) {
                            Log.w(TAG, "Session started for package " + newSession.getPackageName() + " but expected package " + sessionPackage + ". Resetting.");
                            reset();
                            report();
                        } else {
                            currentSession = newSession;

                            PlaybackState playbackState = currentSession.getPlaybackState();
                            if (playbackState == null) {
                                Log.w(TAG, "Unable to determine playback state. Assuming playback is stopped.");
                                currentSession.getTransportControls().play();
                            } else if (playbackState.getState() != PlaybackState.STATE_PLAYING) {
                                Log.i(TAG, "Playback is " + new FieldConstants<>(PlaybackState.class, "STATE_").getName(playbackState.getState()) + ". Sending play command.");
                                currentSession.getTransportControls().play();
                            } else {
                                Log.i(TAG, "Playback resumed");
                            }

                            state = State.COMPLETE;
                            report();
                        }
                    }

                    break;
                case COMPLETE:
                    // We've finished our work. Nothing to do here.
                    break;
            }
        }
    };

    private void startRefreshSwitcherActivity() {
        if (state != State.STARTING_ACTIVITY)
            throw new IllegalStateException("Not in " + State.STARTING_ACTIVITY.name() + " state");

        Intent intent = new Intent(context, RefreshRateSwitcherActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, PendingIntentIds.REFRESH_RATE_SWITCH, intent, PendingIntentIds.PENDING_INTENT_FLAGS);

        // Using a PendingIntent works around the issue where pressing the home button causes a delay in activity starts (https://issuetracker.google.com/issues/36910222)
        // Note that this no longer works in Android 10
        try {
            pendingIntent.send();
        } catch (PendingIntent.CanceledException e) {
            throw new RuntimeException(e);
        }
    }

    private final BroadcastReceiver gotHdmiPlugEvent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int plugState = intent.getIntExtra(AudioManager.EXTRA_AUDIO_PLUG_STATE, -1);
            if (plugState == -1)
                throw new RuntimeException("Got HDMI plug event without plug state");

            if (plugState == 0)
                // We're only interested in plug events
                return;

            if (state != State.SWITCHING_REFRESH_RATE) {
                Log.w(TAG, "Got HDMI plug event when not switching refresh rate. Ignoring.");
                return;
            }

            Log.i(TAG, "Display is back online. Finishing activity");
            RefreshRateSwitcherActivity.requestFinish(context);
            state = State.CLOSING_ACTIVITY;
        }
    };

    private final EventHandler activityChanged = new EventHandler() {
        @Override
        public void update() {
            if (state == State.NOT_STARTED)
                throw new IllegalStateException("Got activity change with state in " + state.name());

            ComponentName newActivity = activityMonitor.currentOrNull();
            if (newActivity == null) {
                Log.w(TAG, "Lost track of the current activity");
                return;
            }

            ComponentName switcherActivity = new ComponentName(context, RefreshRateSwitcherActivity.class);

            switch (state) {
                case STARTING_ACTIVITY:
                    if (!newActivity.equals(switcherActivity)) {
                        Log.w(TAG, "Landed on activity " + newActivity.toShortString() + " but expected " + switcherActivity);
                        reset();
                        report();
                    }
                    break;
                case SWITCHING_REFRESH_RATE:
                    Log.w(TAG, "Landed on activity " + newActivity.toShortString() + " while in state " + State.SWITCHING_REFRESH_RATE);
                    reset();
                    report();
                    break;
                case CLOSING_ACTIVITY:
                    if (newActivity.getPackageName().equals(sessionPackage)) {
                        Log.i(TAG, "Returned to media app without session. Resuming playback.");
                        String commandOutput = ShellCommandRunner.run("media dispatch play");
                        if (commandOutput != null && !commandOutput.isEmpty())
                            throw new RuntimeException("Got unexpected response from play command: " + commandOutput);

                        state = State.COMPLETE;
                        report();
                    } else {
                        if (!newActivity.equals(switcherActivity)) {
                            Log.w(TAG, "Landed on activity " + newActivity.toShortString() + " but expected package " + sessionPackage);
                            reset();
                            report();
                        }
                    }
                    break;
            }
        }
    };

}
