package samuelpalmer.ratesync.processes.service.preview;

import samuelpalmer.ratesync.utilities.eventhandling.EventRegistrarAdaptor;

/**
 * Detects whether the current media session is a preview clip embedded into the media app's user
 * interface. A preview clip could be either a sample of the content or an auto-roll of the content.
 * The main thing that makes something a "preview" is it plays automatically as the user navigates
 * through the UI and is not normal full-screen video playback.
 */
public abstract class PreviewDetector extends EventRegistrarAdaptor {

    /** Returns <c>true</c> if the media session is playing a preview or if we are still working it out, or otherwise <c>false</c> */
    public abstract boolean isPossiblyPreview();

}
