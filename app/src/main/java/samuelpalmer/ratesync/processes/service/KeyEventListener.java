package samuelpalmer.ratesync.processes.service;

import android.content.Context;

import samuelpalmer.ratesync.utilities.eventhandling.EventRegistrarAdaptor;

@SuppressWarnings({"unused", "RedundantSuppression"})
public class KeyEventListener extends EventRegistrarAdaptor {

    private static final String TAG = KeyEventListener.class.getSimpleName();

    private final AdbOutputReader adbOutputReader;

    public KeyEventListener(Context context) {
        adbOutputReader = new AdbOutputReader(context, "getevent -l", listener);
    }

    @Override
    protected void subscribeToAdaptee() {
        adbOutputReader.ensureStarted();
    }

    @Override
    protected void unsubscribeFromAdaptee() {
        adbOutputReader.ensureStopped();
    }

    @SuppressWarnings("FieldCanBeLocal")
    private final AdbOutputReader.Listener listener = line -> {
        if (!hasSubscriptions())
            return;

        // We will also receive events that aren't related to key presses, such as HDMI commands,
        // so we'll filter out non-key-presses
        if (line.contains("KEY_"))
            report();
    };

}
