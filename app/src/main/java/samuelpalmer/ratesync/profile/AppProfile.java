package samuelpalmer.ratesync.profile;

import samuelpalmer.ratesync.processes.service.preview.PreviewDetectionType;

/**
 * Configuration specific to a particular media app
 */
public class AppProfile {
    private final PreviewDetectionType previewType;

    public AppProfile(PreviewDetectionType previewType) {
        this.previewType = previewType;
    }

    public PreviewDetectionType getPreviewType() {
        return previewType;
    }
}
