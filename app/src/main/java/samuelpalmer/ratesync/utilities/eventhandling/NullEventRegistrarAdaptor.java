package samuelpalmer.ratesync.utilities.eventhandling;

/**
 * Does nothing. Use this when you require an {@link EventRegistrarAdaptor} but don't need it to do anything.
 */
@SuppressWarnings({"unused", "RedundantSuppression"})
public class NullEventRegistrarAdaptor extends EventRegistrarAdaptor {

    @Override
    protected void subscribeToAdaptee() {

    }

    @Override
    protected void unsubscribeFromAdaptee() {

    }

}
