package samuelpalmer.ratesync.utilities.eventhandling;

public interface EventHandler {
    void update();
}
