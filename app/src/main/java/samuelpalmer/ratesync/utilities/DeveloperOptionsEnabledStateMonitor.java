package samuelpalmer.ratesync.utilities;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Looper;
import android.provider.Settings;

import androidx.core.os.HandlerCompat;

import samuelpalmer.ratesync.utilities.eventhandling.EventRegistrarAdaptor;

/**
 * Detects when and whether the user has enabled "Developer options" in the Android settings app
 */
public class DeveloperOptionsEnabledStateMonitor extends EventRegistrarAdaptor {

    @SuppressWarnings({"unused", "RedundantSuppression"})
    private static final String TAG = DeveloperOptionsEnabledStateMonitor.class.getSimpleName();

    private final ContentResolver contentResolver;

    private boolean isUpToDate = false;
    private boolean wasEnabled;

    public DeveloperOptionsEnabledStateMonitor(Context context) {
        contentResolver = context.getContentResolver();
    }

    /** Whether "Developer options" is enabled in the Android settings app */
    public boolean isEnabled() {
        if (isUpToDate)
            return wasEnabled;
        else
            return poll();
    }

    @Override
    protected void subscribeToAdaptee() {
        contentResolver.registerContentObserver(Settings.Global.getUriFor(Settings.Global.DEVELOPMENT_SETTINGS_ENABLED), false, observer);
    }

    @Override
    protected void unsubscribeFromAdaptee() {
        contentResolver.unregisterContentObserver(observer);
        isUpToDate = false;
    }

    private final ContentObserver observer = new ContentObserver(HandlerCompat.createAsync(Looper.getMainLooper())) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);

            if (hasSubscriptions()) {
                boolean isEnabled = poll();
                if (!isUpToDate || isEnabled != wasEnabled) {
                    wasEnabled = isEnabled;
                    isUpToDate = true;
                    report();
                }
            }
        }
    };

    private boolean poll() {
        int developmentSettings = Settings.Global.getInt(contentResolver, Settings.Global.DEVELOPMENT_SETTINGS_ENABLED, 0);
        return developmentSettings == 1;
    }

}
