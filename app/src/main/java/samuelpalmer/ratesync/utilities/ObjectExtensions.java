package samuelpalmer.ratesync.utilities;

public final class ObjectExtensions {

    /**
     * Compares by reference if either is {@code null}. Otherwise compares using {@link Object#equals(Object)}.
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean areEqual(Object a, Object b) {
        return a == null || b == null ? a == b : a.equals(b);
    }

}
