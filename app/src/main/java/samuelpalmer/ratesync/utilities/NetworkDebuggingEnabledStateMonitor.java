package samuelpalmer.ratesync.utilities;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Looper;
import android.provider.Settings;

import androidx.core.os.HandlerCompat;

import samuelpalmer.ratesync.utilities.eventhandling.EventRegistrarAdaptor;

/**
 * Detects when and whether the NVIDIA Shield TV's "Network debugging" option is turned on
 * in the developer options screen
 */
public class NetworkDebuggingEnabledStateMonitor extends EventRegistrarAdaptor {

    @SuppressWarnings({"unused", "RedundantSuppression"})
    private static final String TAG = NetworkDebuggingEnabledStateMonitor.class.getSimpleName();
    private static final String SETTING_NETWORK_DEBUGGING = "adb_tcp_enabled";

    private final ContentResolver contentResolver;

    private boolean isUpToDate = false;
    private boolean wasEnabled;

    public NetworkDebuggingEnabledStateMonitor(Context context) {
        contentResolver = context.getContentResolver();
    }

    /** Whether the NVIDIA Shield TV's "Network debugging" option is turned on in the developer options screen */
    public boolean isEnabled() {
        if (isUpToDate)
            return wasEnabled;
        else
            return poll();
    }

    @Override
    protected void subscribeToAdaptee() {
        contentResolver.registerContentObserver(Settings.Global.getUriFor(SETTING_NETWORK_DEBUGGING), false, observer);
    }

    @Override
    protected void unsubscribeFromAdaptee() {
        contentResolver.unregisterContentObserver(observer);
        isUpToDate = false;
    }

    private final ContentObserver observer = new ContentObserver(HandlerCompat.createAsync(Looper.getMainLooper())) {
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);

            if (hasSubscriptions()) {
                boolean isEnabled = poll();
                if (!isUpToDate || isEnabled != wasEnabled) {
                    wasEnabled = isEnabled;
                    isUpToDate = true;
                    report();
                }
            }
        }
    };

    private boolean poll() {
        int networkDebugging = Settings.Global.getInt(contentResolver, SETTING_NETWORK_DEBUGGING, 0);
        return networkDebugging == 1;
    }

}
