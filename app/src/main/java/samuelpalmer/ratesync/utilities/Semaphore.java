package samuelpalmer.ratesync.utilities;

import android.net.LocalServerSocket;

import java.io.IOException;

import samuelpalmer.ratesync.BuildConfig;

/**
 * Provides mutual exclusion for multiple processes and/or threads. Note that this class itself
 * isn't "thread-safe"; you shouldn't use one instance in multiple threads. Instead, use a different
 * instance in each thread but with the same identifier.
 */
public class Semaphore implements AutoCloseable {

    private LocalServerSocket lock;

    /**
     * @param lockName Uniquely identifies the lock within this app
     */
    public Semaphore(String lockName, long timeoutMillis) throws InterruptedException {
        long timeout = System.currentTimeMillis() + timeoutMillis;
        String globalName = BuildConfig.APPLICATION_ID + ":" + lockName;

        do {
            try {
                lock = new LocalServerSocket(globalName);
            } catch (IOException e) {
                //noinspection BusyWait
                Thread.sleep(1);

                if (System.currentTimeMillis() > timeout)
                    throw new RuntimeException("Locking took longer than " + timeoutMillis + "ms.");
            }
        } while (lock == null);
    }

    @Override
    public void close() {
        if (lock == null)
            throw new IllegalStateException("Already closed");

        try {
            lock.close();
        } catch (IOException e) {
            // This is an error because the lock might be held permanently if this happens :(
            throw new RuntimeException("Error closing semaphore", e);
        }

        lock = null;
    }

}
