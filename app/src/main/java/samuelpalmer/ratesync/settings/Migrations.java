package samuelpalmer.ratesync.settings;

import android.util.SparseArray;

/**
 * Contains the list of upgrade scripts
 */
class Migrations {

    static SparseArray<Migration> buildMap() {
        return new SparseArray<>();
    }

}