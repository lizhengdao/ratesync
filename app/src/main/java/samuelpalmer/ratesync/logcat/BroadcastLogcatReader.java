package samuelpalmer.ratesync.logcat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 * Listens for simulated logcat commands for use in local development.
 * This is needed because the official Android emulators block local ADB connections.
 */
class BroadcastLogcatReader extends LogcatReader {

    // If you change these, make sure to also change them in the Shield Simulator app
    private static final String ACTION_BROADCAST_LOG = "samuelpalmer.ratesync.dev.action.LOG";
    private static final String EXTRA_LOG_MESSAGE = "samuelpalmer.ratesync.dev.extra.MESSAGE";

    private final Context context;
    private final Args args;

    private boolean isRunning;

    public BroadcastLogcatReader(Context context, Args args, Listener listener) {
        super(listener);
        this.context = context;
        this.args = args;

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_BROADCAST_LOG);

        context.registerReceiver(broadcastReceiver, filter);

        isRunning = true;
    }

    @Override
    public void stop() {
        isRunning = false;

        context.unregisterReceiver(broadcastReceiver);
    }

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!isRunning)
                return;

            String logMessage = intent.getStringExtra(EXTRA_LOG_MESSAGE);
            if (logMessage == null)
                throw new RuntimeException("Received log message broadcast without message");

            // Ignoring other logcat args to keep things simple

            if (args.filter.matcher(logMessage).find())
                listener.onLogLine(logMessage);
        }
    };

}
