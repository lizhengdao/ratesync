package samuelpalmer.ratesync.logcat;

import android.content.Context;

import androidx.annotation.MainThread;

import java.text.SimpleDateFormat;
import java.util.Locale;

import samuelpalmer.ratesync.processes.service.AdbOutputReader;

/**
 * Runs logcat over a local connection to ADB. Suitable for the NVIDIA Shield TV devices.
 */
class AdbLogcatReader extends LogcatReader {

    @SuppressWarnings({"unused", "RedundantSuppression"})
    private static final String TAG = AdbLogcatReader.class.getSimpleName();

    private AdbOutputReader adbOutputReader;

    @MainThread
    public AdbLogcatReader(Context context, Args args, LogcatReader.Listener listener) {
        super(listener);

        String formattedArgs = formatArgs(args);
        adbOutputReader = new AdbOutputReader(context, "logcat" + formattedArgs, listener::onLogLine);
        adbOutputReader.ensureStarted();
    }

    @Override
    @MainThread
    public void stop() {
        adbOutputReader.ensureStopped();
        adbOutputReader = null;
    }

    private static String formatArgs(Args args) {
        StringBuilder result = new StringBuilder();

        if (args.buffer != null) {
            result.append(" -b ");
            result.append(args.buffer);
        }

        if (args.format != null) {
            result.append(" -v ");
            result.append(args.format);
        }

        if (args.filter != null) {
            result.append(" -e '");
            result.append(args.filter.pattern());
            result.append("'");
        }

        if (args.since != null) {
            String formattedStartTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US).format(args.since);

            result.append(" -T '");
            result.append(formattedStartTime);
            result.append("'");
        }

        return result.toString();
    }

}
