package samuelpalmer.ratesync.logcat;

import android.content.Context;

import androidx.annotation.MainThread;

import java.util.Date;
import java.util.regex.Pattern;

import samuelpalmer.ratesync.BuildConfig;
import samuelpalmer.ratesync.utilities.DeviceInfo;

//import samuelpalmer.ratesync.BuildConfig;

/**
 * Runs logcat and reports its output in real-time as it arrives.
 */
public abstract class LogcatReader {

    public interface Listener {
        /**
         * Called on the main thread after a complete log line arrives.
         * Will never be called after a call to {@link #stop()}.
         */
        @MainThread
        void onLogLine(String logLine);
    }

    final Listener listener;

    /**
     * Begins listening to logs
     * @param args Command-line arguments to pass to logcat
     */
    public static LogcatReader start(Context context, Args args, Listener listener) {
        if (BuildConfig.DEBUG && DeviceInfo.isEmulator())
            return new BroadcastLogcatReader(context, args, listener);
        else
            return new AdbLogcatReader(context, args, listener);
    }

    public abstract void stop();

    /** Command-line argumemnts to pass to logcat */
    public static class Args {
        public String buffer;
        public String format;
        public Pattern filter;
        public Date since;
    }

    LogcatReader(Listener listener) {
        this.listener = listener;
    }

}
