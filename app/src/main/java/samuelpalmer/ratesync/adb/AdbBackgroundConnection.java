package samuelpalmer.ratesync.adb;

import android.net.TrafficStats;
import android.os.Handler;
import android.os.Looper;
import android.system.ErrnoException;
import android.system.OsConstants;
import android.util.Log;

import androidx.annotation.MainThread;
import androidx.annotation.WorkerThread;
import androidx.core.os.HandlerCompat;

import com.tananaev.adblib.AdbConnection;
import com.tananaev.adblib.AdbCrypto;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;

import samuelpalmer.ratesync.utilities.State;

/**
 * Establishes a single ADB connection and runs a single command on it. Not re-using a connection
 * for multiple separate commands/streams because I'm not sure how thread-safe AdbLib is.
 */
public class AdbBackgroundConnection extends State {

    private static final String TAG = AdbBackgroundConnection.class.getSimpleName();

    /** Just setting this to 1 since we don't need to analyse data usage for ADB connections */
    private static final int DATA_USAGE_TAG = 1;

    /**
     * Maximum time allowed for the call to {@link AdbConnection#connect()} to complete, in milliseconds.
     * If the ADB server hasn't permanently authorised us, then it may stop responding and leave the
     * call to {@link AdbConnection#connect()} hanging indefinitely, or it may start responding
     * once the user presses "OK" in the auth popup, but only sometimes. So it's best to treat any
     * delay as "bad" so that the user has to give us permanent auth, which is the only way we
     * can make the connection work reliably every time.
     */
    private static final int TIMEOUT_CONNECT_MILLIS = 500;

    /**
     * Maximum time allowed for the connection thread to finish after we request it to, in milliseconds.
     * The connection thread and callback are expected to handle the interruption and finish up,
     * so something has probably gone wrong if this timeout is hit.
     */
    private static final int TIMEOUT_CONNECTION_CLOSE_MILLIS = 10000;

    /**
     * Maximum time allowed for the network socket to connect to the ADB server, in milliseconds.
     * The connection should be instant since the server runs on the local machine,
     * so something has probably gone wrong if this timeout is hit.
     */
    private static final int TIMEOUT_SOCKET_CONNECTION_MILLIS = 2500;

    public interface ConnectionCallbacks {
        /**
         * Called in a worker thread after the connection is established. This won't be called if
         * the connection fails. Once the connection is stopped, then this thread will be
         * interrupted. You may either throw an {@link InterruptedException} or return from the
         * method.
         */
        @WorkerThread
        void onConnected(AdbConnection connection) throws InterruptedException;

        /**
         * Called on the main thread if the connection is not established because the user has not
         * yet granted permanent ADB authorisation to the app
         */
        @MainThread
        void onPermanentAuthMissing();

        /**
         * Called on the main thread if the connection is not established because network debugging
         * is not available
         */
        @MainThread
        void onNetworkDebuggingDisabled();
    }

    private final Handler handler;
    private final ConnectionCallbacks connectionCallbacks;

    private Thread thread;
    private volatile boolean isRunning;

    @MainThread
    public AdbBackgroundConnection(ConnectionCallbacks connectionCallbacks) {
        this.connectionCallbacks = connectionCallbacks;
        handler = HandlerCompat.createAsync(Looper.getMainLooper());
    }

    @MainThread
    @Override
    protected void start() {
        isRunning = true;

        Log.d(TAG, "Starting background connection thread");
        thread = new Thread(() -> connect(connectionCallbacks), getClass().getSimpleName());
        thread.start();
    }

    @MainThread
    @Override
    protected void stop() {
        // Signalling that we're finishing up so the thread expects the interruption
        isRunning = false;
        thread.interrupt();
        try {
            // Waiting for the thread to finish to ensure we cleanly disconnect from ADB
            // Don't want to leave anything behind in case it causes issues
            thread.join(TIMEOUT_CONNECTION_CLOSE_MILLIS);
        } catch (InterruptedException e) {
            // We didn't get wait for the thread to finish, but we can live with that
            throw new RuntimeException("Interrupted while waiting for ADB reader thread to clean up", e);
        }

        if (thread.isAlive())
            throw new RuntimeException("Took longer than " + TIMEOUT_CONNECTION_CLOSE_MILLIS + "ms for ADB connection thread to die");

        Log.d(TAG, "Background connection thread finished");

        thread = null;

        // Now that the worker thread is gone, we can safely remove callbacks without worrying about
        // it re-adding them
        handler.removeCallbacks(connectTimedOut);
        handler.removeCallbacks(connectionRefused);
    }

    /**
     * Runs the bulk of the work in a worker thread so we don't hold up the UI and since Android
     * won't let us do network IO on the main thread
     */
    @WorkerThread
    private void connect(ConnectionCallbacks connectionCallbacks) {
        try {
            // Fetching the key-pair in the worker thread since it can be slow
            AdbCrypto crypto = AdbCryptoStore.getAdbCrypto();

            Log.d(TAG, "Connecting to ADB server");

            // Connect to ADB server
            try (Socket socket = new Socket()) {
                // Setting tag to fix android.os.strictmode.UntaggedSocketViolation
                TrafficStats.setThreadStatsTag(DATA_USAGE_TAG);
                socket.connect(new InetSocketAddress("localhost", 5555), TIMEOUT_SOCKET_CONNECTION_MILLIS);

                try (AdbConnection connection = AdbConnection.create(socket, crypto)) {
                    // The ADB server may stop responding if auth is required, which will leave our
                    // thread hanging indefinitely, so we need to abort from the main thread if it looks
                    // to be taking too long
                    handler.postDelayed(connectTimedOut, TIMEOUT_CONNECT_MILLIS);
                    try {
                        // This will prompt the user for ADB auth if they haven't granted permanent
                        // authorisation
                        connection.connect();
                    } finally {
                        handler.removeCallbacks(connectTimedOut);
                    }

                    // We'll check for an interruption before calling the callback to make sure the
                    // callback isn't called while we're stopping
                    if (Thread.interrupted())
                        throw new InterruptedException("Interrupted after establishing ADB connection");

                    Log.d(TAG, "Connected to ADB server");
                    connectionCallbacks.onConnected(connection);

                    // Note that we may not get to this line because the callback might throw an InterruptedException
                }
            } catch (IOException e) {
                if (isConnectionRefused(e)) {
                    // Network debugging is probably switched off
                    handler.post(connectionRefused);
                } else
                    throw new RuntimeException("ADB comms error", e);
            }
        } catch (InterruptedException e) {
            if (isRunning)
                throw new RuntimeException("Got unexpected interruption in ADB server connection thread", e);

            // We've aborted the connection from the main thread, so we can proceed to close the connection
        }
    }

    private final Runnable connectTimedOut = new Runnable() {
        @MainThread
        @Override
        public void run() {
            if (!isStarted()) {
                Log.w(TAG, "Got connection timeout when not started. Ignoring.");
                return;
            }

            Log.d(TAG, "Call to connect() timed out. We are probably not permanently authorised with the ADB server.");
            connectionCallbacks.onPermanentAuthMissing();
        }
    };

    private final Runnable connectionRefused = new Runnable() {
        @MainThread
        @Override
        public void run() {
            if (!isStarted()) {
                Log.w(TAG, "Got connection refused when not started. Ignoring.");
                return;
            }

            Log.d(TAG, "Connection was refused. Network debugging is probably disabled.");
            connectionCallbacks.onNetworkDebuggingDisabled();
        }
    };

    /**
     * Determines whether the given exception is a "Connection refused" error. This typically means
     * network debugging is disabled on the target device.
     */
    private static boolean isConnectionRefused(Exception e) {
        if (!(e instanceof ConnectException))
            return false;

        Throwable cause = e.getCause();
        if (!(cause instanceof ErrnoException))
            return false;

        ErrnoException errnoException = (ErrnoException) cause;
        return errnoException.errno == OsConstants.ECONNREFUSED;
    }

}
