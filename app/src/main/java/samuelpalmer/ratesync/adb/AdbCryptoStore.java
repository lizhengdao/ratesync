package samuelpalmer.ratesync.adb;

import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

import com.tananaev.adblib.AdbBase64;
import com.tananaev.adblib.AdbCrypto;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;

import samuelpalmer.ratesync.utilities.Semaphore;

/**
 * Takes care of storage and access to our key-pair that is used to authenticate against the ADB
 * server. Uses AndroidKeyStore since this makes it hard for other users and apps to access the
 * private key, while saving us from needing to store secrets in the file system or app code.
 */
public class AdbCryptoStore {

    private static final String TAG = AdbCryptoStore.class.getSimpleName();

    private static final String PROVIDER_ANDROID_KEY_STORE = "AndroidKeyStore";

    /**
     * The alias of our key-pair inside AndroidKeyStore. Note that this only needs to be unique
     * within our own application since apps can't access each others' AndroidKeyStore entries.
     */
    private static final String KEY_STORE_ALIAS = "adb";

    @Nullable
    private static volatile AdbCrypto adbCrypto;

    /**
     * Fetches this app's RSA key-pair, or creates one if it doesn't exist.
     * The key-pair will be the same across app restarts and device restarts.
     * Note that this may take a couple of seconds to run, particularly if the
     * key-pair is generated for the first time, so you should not call this
     * from the main thread.
     */
    @WorkerThread
    public synchronized static AdbCrypto getAdbCrypto() throws InterruptedException {
        if (adbCrypto != null)
            return adbCrypto;

        // If two processes run the below code, then there is a risk that both processes
        // will generate a new key-pair at the same time. AndroidKeyStore will replace the first
        // generated key-pair with the second, so if the user authorises the first one, then it
        // will be unused and the user will then also need to authorise the second one. So we are
        // using a semaphore to ensure only one process does this at a time.

        try (Semaphore ignored = new Semaphore("adbcrypto", 10000)) {
            KeyPair keyPair = fetchKeyPair();
            if (keyPair == null) {
                Log.i(TAG, "Creating ADB key-pair");
                keyPair = createNewKeyPair();
            }

            adbCrypto = AdbCrypto.loadAdbKeyPair(androidBase64, keyPair);
        }

        return adbCrypto;
    }

    private static KeyPair fetchKeyPair() {
        try {
            KeyStore keyStore = KeyStore.getInstance(PROVIDER_ANDROID_KEY_STORE);
            keyStore.load(null);

            // We need to check if the key and its components are in the store before attempting to
            // retrieve them so we don't get a warning logged on Android 9: https://stackoverflow.com/q/52024752/238753
            // The warning could potentially be caused by a number of issues, such as uninstalling
            // and reinstalling the app, or restarting the device.
            if (!keyStore.containsAlias(KEY_STORE_ALIAS))
                return null;

            PrivateKey privateKey = (PrivateKey) keyStore.getKey(KEY_STORE_ALIAS, null);
            if (privateKey == null)
                return null;

            Certificate certificate = keyStore.getCertificate(KEY_STORE_ALIAS);
            if (certificate == null)
                return null;

            PublicKey publicKey = certificate.getPublicKey();
            if (publicKey == null)
                return null;

            return new KeyPair(publicKey, privateKey);
        } catch (IOException | CertificateException | NoSuchAlgorithmException | KeyStoreException | UnrecoverableEntryException e) {
            throw new RuntimeException("Error fetching key-pair", e);
        }
    }

    private static KeyPair createNewKeyPair() {
        KeyGenParameterSpec parameterSpec = new KeyGenParameterSpec.Builder(KEY_STORE_ALIAS, KeyProperties.PURPOSE_SIGN)
                .setKeySize(AdbCrypto.KEY_LENGTH_BITS)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE) // To work around the error "android.security.KeyStoreException: Incompatible padding mode
                .setRandomizedEncryptionRequired(false) // To work around the error "Randomized encryption (IND-CPA) required but may be violated by padding scheme: NoPadding"
                .setDigests(KeyProperties.DIGEST_NONE) // To work around the error "android.security.KeyStoreException: Incompatible digest"
                // Not using strongbox since NVIDIA Shield TV doesn't have one and if the keystore is compromised then the attacker would already have root/ADB access anyway
                .setUnlockedDeviceRequired(false) // The user's Android TV box might not even have a lock on it
                .setUserAuthenticationRequired(false) // The user's Android TV box might not even have a lock on it
                .build();

        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA", PROVIDER_ANDROID_KEY_STORE);
            keyGen.initialize(parameterSpec);
            return keyGen.generateKeyPair();
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | NoSuchProviderException e) {
            throw new RuntimeException("Error creating key-pair", e);
        }
    }

    private static final AdbBase64 androidBase64 = data -> Base64.encodeToString(data, Base64.NO_WRAP);

    private AdbCryptoStore() {}

}
