package samuelpalmer.ratesync;

import android.app.AppOpsManager;
import android.content.Context;

import samuelpalmer.ratesync.processes.service.NotificationListener;
import samuelpalmer.ratesync.utilities.AppOpMonitor;
import samuelpalmer.ratesync.utilities.DeveloperOptionsEnabledStateMonitor;
import samuelpalmer.ratesync.utilities.NetworkDebuggingEnabledStateMonitor;
import samuelpalmer.ratesync.utilities.NotificationListenerEnabledStateMonitor;
import samuelpalmer.ratesync.utilities.eventhandling.EventHandler;
import samuelpalmer.ratesync.utilities.eventhandling.EventRegistrarAdaptor;

/**
 * Keeps track of whether all of this app's permissions are granted, with the exception of the ADB
 * permanent authorisation since that can't be monitored
 */
public class AllPermissionsMonitor extends EventRegistrarAdaptor {

    private final AppOpMonitor usageOpMonitor;
    private final NotificationListenerEnabledStateMonitor notificationListenerEnabledStateMonitor;
    private final AppOpMonitor displayOverOtherAppsMonitor;
    private final DeveloperOptionsEnabledStateMonitor developerOptionsEnabledStateMonitor;
    private final NetworkDebuggingEnabledStateMonitor networkDebuggingEnabledStateMonitor;

    private boolean isUpToDate = false;
    private boolean hadPermissions;

    public AllPermissionsMonitor(Context context) {
        usageOpMonitor = new AppOpMonitor(context, AppOpsManager.OPSTR_GET_USAGE_STATS);
        notificationListenerEnabledStateMonitor = new NotificationListenerEnabledStateMonitor(context, NotificationListener.class);
        displayOverOtherAppsMonitor = new AppOpMonitor(context, AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW);
        developerOptionsEnabledStateMonitor = new DeveloperOptionsEnabledStateMonitor(context);
        networkDebuggingEnabledStateMonitor = new NetworkDebuggingEnabledStateMonitor(context);
    }

    /**
     * Whether all of this app's permissions are granted (excluding ADB permanent authorisation)
     */
    public boolean hasPermissions() {
        if (isUpToDate)
            return hadPermissions;
        else
            return poll();
    }

    @Override
    protected void subscribeToAdaptee() {
        usageOpMonitor.subscribe(permissionChanged);
        notificationListenerEnabledStateMonitor.subscribe(permissionChanged);
        displayOverOtherAppsMonitor.subscribe(permissionChanged);
        developerOptionsEnabledStateMonitor.subscribe(permissionChanged);
        networkDebuggingEnabledStateMonitor.subscribe(permissionChanged);
    }

    @Override
    protected void unsubscribeFromAdaptee() {
        networkDebuggingEnabledStateMonitor.unsubscribe(permissionChanged);
        developerOptionsEnabledStateMonitor.unsubscribe(permissionChanged);
        displayOverOtherAppsMonitor.unsubscribe(permissionChanged);
        notificationListenerEnabledStateMonitor.unsubscribe(permissionChanged);
        usageOpMonitor.unsubscribe(permissionChanged);
        isUpToDate = false;
    }

    private final EventHandler permissionChanged = new EventHandler() {
        @Override
        public void update() {
            if (hasSubscriptions()) {
                boolean hasPermissions = poll();
                if (!isUpToDate || hasPermissions != hadPermissions) {
                    hadPermissions = hasPermissions;
                    isUpToDate = true;
                    report();
                }
            }
        }
    };

    public boolean poll() {
        return usageOpMonitor.hasPermission()
                && notificationListenerEnabledStateMonitor.isEnabled()
                && displayOverOtherAppsMonitor.hasPermission()
                && developerOptionsEnabledStateMonitor.isEnabled()
                && networkDebuggingEnabledStateMonitor.isEnabled();
    }

}
