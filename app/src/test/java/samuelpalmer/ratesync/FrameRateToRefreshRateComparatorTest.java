package samuelpalmer.ratesync;

import org.junit.Test;

import samuelpalmer.ratesync.processes.service.FrameRateToRefreshRateComparator;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

public class FrameRateToRefreshRateComparatorTest {

    @Test
    public void treatsEqualRefreshRatesAsEqual() {
        // Using a frame rate that is very undesirable because it both drops frames and does so unevenly, in order to throw things off
        // Using non-integer refresh rates to further complicate things
        int result = compare(45.01f, 29.99f, 29.99f);
        assertThat(result, equalTo(0));
    }

    @Test
    public void prefersToAvoidDroppingFramesAtAllCosts() {
        // Frame drops are considered the worst case because some original video content is lost
        // Using a very close refresh rate that will only drop a tiny amount of frames to make it a tempting choice
        // Using a far off refresh rate that won't drop any frames but has very bad uneven frame doubling to also make it a tough choice
        assertThatAIsBetter(30, 45, 29.95f);
    }

    @Test
    public void prefersTheLeastFrameDropsWhenNecessary() {
        // When we have to drop frames, then it's better to pick the refresh rate that drops less
        // frames than the other
        assertThatAIsBetter(60, 30, 29.97f);
        assertThatAIsBetter(60, 59, 58f);
    }

    @Test
    public void prefersLessJudder() {
        // When there are no frame drops, but an exact match isn't available, then it's better to
        // use a mode that has the least amount of fractional frame doubling so the motion is even.
        // Even motion appears slightly smoother and clearer than juddery motion.
        assertThatAIsBetter(30, 60, 59.94f);
        assertThatAIsBetter(29.97f, 59.94f, 60);
        assertThatAIsBetter(23.976f, 24, 25);
        assertThatAIsBetter(23.976f, 24, 25);
        assertThatAIsBetter(23.976f, 50, 29.97f);

        // It's better to double every 3rd frame than every 2nd frame
        assertThatAIsBetter(40, 53.33f, 60);

        // Judder must be more about how much of the time things are *off* as opposed to how much of
        // the time we have to do a frame double
        assertThatAIsBetter(31, 60 /* High doubling, rare single frame */, 40 /* Low  doubling, occasional double frame */);
    }

    @Test
    public void looksAtJudderPercentageInsteadOfFrameDoublePercentage() {
        assertThatAIsBetter(29.97f, 60 /* High doubling, low judder */, 39.96f /* Low doubling, high judder */ );
    }

    @Test
    public void prefersLowerRefreshRate() {
        // When there are no issues of frame dropping or uneven frame doubling, then lower refresh
        // rates are preferred because they are more likely to support advanced display features
        // such as HDR or motion interpolation
        assertThatAIsBetter(30, 30, 60);
        assertThatAIsBetter(30, 60, 120);
    }

    private static void assertThatAIsBetter(float frameRateFps, float refreshRateHz1, float refreshRateHz2) {
        int resultNormalOrder = compare(frameRateFps, refreshRateHz1, refreshRateHz2);
        assertThat(resultNormalOrder, lessThan(0));

        int resultReverseOrder = compare(frameRateFps, refreshRateHz2, refreshRateHz1);
        assertThat(resultReverseOrder, greaterThan(0));
    }

    private static int compare(float frameRateFps, float refreshRateHz1, float refreshRateHz2) {
        FrameRateToRefreshRateComparator comparator = new FrameRateToRefreshRateComparator(frameRateFps);
        return comparator.compare(refreshRateHz1, refreshRateHz2);
    }

}
