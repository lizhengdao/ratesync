package samuelpalmer.shieldsimulator;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Spinner frameRateSpinner = findViewById(R.id.frame_rate_spinner);
        ArrayAdapter<FrameRate> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, FrameRate.all);
        frameRateSpinner.setAdapter(adapter);

        Button setFrameRateButton = findViewById(R.id.set_frame_rate_button);
        setFrameRateButton.setOnClickListener(v -> {
            FrameRate selectedFrameRate = (FrameRate) frameRateSpinner.getSelectedItem();
            NvidiaShieldLogSimulator.setStartVideoFrameRate(MainActivity.this, selectedFrameRate);
        });
    }

}
