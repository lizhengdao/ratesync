package samuelpalmer.shieldsimulator;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class NvidiaShieldLogSimulator {

    // If you change these, make sure to also change them in the main app
    private static final String ACTION_BROADCAST_LOG = "samuelpalmer.ratesync.dev.action.LOG";
    private static final String EXTRA_LOG_MESSAGE = "samuelpalmer.ratesync.dev.extra.MESSAGE";

    public static void setStartVideoFrameRate(Context context, FrameRate frameRate) {
        log(context, "NvOsDebugPrintf", "NVMEDIA: FrameRate = " + frameRate.nvidiaLogStartUp);
    }

    @SuppressWarnings("SameParameterValue")
    private static void log(Context context, String tag, String message) {
        // Sending the message as a broadcast since it's too hard to read system logs when running
        // on official emulator
        Intent intent = new Intent();
        intent.setAction(ACTION_BROADCAST_LOG);
        intent.putExtra(EXTRA_LOG_MESSAGE, message);
        context.sendBroadcast(intent);

        Log.d(tag, message);
    }

    private NvidiaShieldLogSimulator() {}

}
